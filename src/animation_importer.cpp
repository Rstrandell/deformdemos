#include "animation_importer.h"
#include "tbb/tbb.h"

void AnimationImporter::VertexBoneData::AddBoneData(unsigned int BoneID, float Weight)
{
	for (unsigned int i = 0; i < ARRAY_SIZE_IN_ELEMENTS(IDs); i++) {
		if (Weights[i] == 0.0) {
			IDs[i] = BoneID;
			Weights[i] = Weight;
			return;
		}
	}

	// should never get here - more bones than we have space for
	assert(0);
}

bool AnimationImporter::LoadAnimation(string file_path, vector<float> & vertices, vector<float> & normals, vector<int> & triangles,
									  float t_x, float t_y, float t_z, 
									  float r_angle, float r_x, float r_y, float r_z, 
									  float s_x, float s_y, float s_z)
{
	m_pScene = m_Importer.ReadFile(file_path, aiProcess_Triangulate | aiProcess_OptimizeGraph |
											  aiProcess_OptimizeMeshes | aiProcess_GenSmoothNormals | aiProcess_FindDegenerates | aiProcess_FindInstances | 
	aiProcess_ImproveCacheLocality | aiProcess_JoinIdenticalVertices | aiProcess_FindInvalidData | aiProcess_ValidateDataStructure);

	if (m_pScene)
	{
		Eigen::Matrix4f translation = Eigen::Matrix4f::Identity();
		translation(0, 3) = -t_x;
		translation(1, 3) = -t_y;
		translation(2, 3) = -t_z;

		Eigen::Matrix4f rotation = Eigen::Matrix4f::Identity();
		rotation.block<3, 3>(0, 0) = Eigen::AngleAxis<Scalar>(r_angle, Vector3x(r_x, r_y, r_z)).toRotationMatrix();

		Eigen::Matrix4f scale = Eigen::Matrix4f::Identity();
		scale(0, 0) = 1.0f / s_x;
		scale(1, 1) = 1.0f / s_y;
		scale(2, 2) = 1.0f / s_z;

		m_GlobalInverseTransform = scale * rotation * translation * AiMatrix4x4ToEigenMatrix4f(m_pScene->mRootNode->mTransformation);
		m_GlobalInverseTransform = m_GlobalInverseTransform.inverse();

		bool result = InitFromScene(m_pScene);

		for (int i = 0; i < Positions.size(); i++)
		{
			Eigen::Vector4f p = m_GlobalInverseTransform * Eigen::Vector4f(Positions[i].x(), Positions[i].y(), Positions[i].z(), 1.0f);

			vertices.push_back(p.x());
			vertices.push_back(p.y());
			vertices.push_back(p.z());

			normals.push_back(Normals[i].x());
			normals.push_back(Normals[i].y());
			normals.push_back(Normals[i].z());
		}

		for (int i = 0; i < Indices.size(); i++)
		{
			triangles.push_back(Indices[i]);
		}

		return result;
	}
	else 
	{
		printf("Error parsing '%s': '%s'\n", file_path.c_str(), m_Importer.GetErrorString());
		return false;
	}
}

bool AnimationImporter::InitFromScene(const aiScene *pScene)
{
	m_Entries.resize(pScene->mNumMeshes);


	unsigned int NumVertices = 0;
	unsigned int NumIndices = 0;

	// Count the number of vertices and indices
	for (unsigned int i = 0; i < m_Entries.size(); i++) {
		m_Entries[i].MaterialIndex = pScene->mMeshes[i]->mMaterialIndex;
		m_Entries[i].NumIndices = pScene->mMeshes[i]->mNumFaces * 3;
		m_Entries[i].BaseVertex = NumVertices;
		m_Entries[i].BaseIndex = NumIndices;

		NumVertices += pScene->mMeshes[i]->mNumVertices;
		NumIndices += m_Entries[i].NumIndices;
	}

	// Reserve space in the vectors for the vertex attributes and indices
	Positions.reserve(NumVertices);
	Normals.reserve(NumVertices);
	Bones.resize(NumVertices);
	Indices.reserve(NumIndices);



	// Initialize the meshes in the scene one by one
	for (unsigned int i = 0; i < m_Entries.size(); i++) {
		
		const aiMesh* paiMesh = pScene->mMeshes[i];
		InitMesh(i, paiMesh, Positions, Normals, Bones, Indices);
	}

	return true;
}

void AnimationImporter::InitMesh(unsigned int MeshIndex,
	const aiMesh* paiMesh,
	vector<Vector3f>& Positions,
	vector<Vector3f>& Normals,
	vector<VertexBoneData>& Bones,
	vector<int>& Indices)
{
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);
	// Populate the vertex attribute vectors

	int index_offset = (int) Positions.size();

	for (unsigned int i = 0; i < paiMesh->mNumVertices; i++) {
		const aiVector3D* pPos = &(paiMesh->mVertices[i]);
		const aiVector3D* pNormal = &(paiMesh->mNormals[i]);
		const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;


		Positions.push_back(Vector3f(pPos->x, pPos->y, pPos->z));
		Normals.push_back(Vector3f(pNormal->x, pNormal->y, pNormal->z));
	}

	LoadBones(MeshIndex, paiMesh, Bones);

	// Populate the index buffer

	for (unsigned int i = 0; i < paiMesh->mNumFaces; i++) {
		const aiFace& Face = paiMesh->mFaces[i];
		assert(Face.mNumIndices == 3);
		Indices.push_back(Face.mIndices[0] + index_offset);
		Indices.push_back(Face.mIndices[1] + index_offset);
		Indices.push_back(Face.mIndices[2] + index_offset);
	}
}

void AnimationImporter::LoadBones(unsigned int MeshIndex, const aiMesh* pMesh, vector<VertexBoneData>& Bones)
{
	for (unsigned int i = 0; i < pMesh->mNumBones; i++) 
	{
		unsigned int BoneIndex = 0;
		string BoneName(pMesh->mBones[i]->mName.C_Str());
		if (m_BoneMapping.find(BoneName) == m_BoneMapping.end()) 
		{
			// Allocate an index for a new bone
			BoneIndex = m_NumBones;
			m_NumBones++;
			BoneInfo bi;
			m_BoneInfo.push_back(bi);
			m_BoneInfo[BoneIndex].BoneOffset = AiMatrix4x4ToEigenMatrix4f(pMesh->mBones[i]->mOffsetMatrix);
			m_BoneMapping[BoneName] = BoneIndex;
		}
		else 
		{
			BoneIndex = m_BoneMapping[BoneName];
		}

		for (unsigned int j = 0; j < pMesh->mBones[i]->mNumWeights; j++) 
		{
			unsigned int VertexID = m_Entries[MeshIndex].BaseVertex + pMesh->mBones[i]->mWeights[j].mVertexId;
			float Weight = pMesh->mBones[i]->mWeights[j].mWeight;
			Bones[VertexID].AddBoneData(BoneIndex, Weight);
		}
	}
}

unsigned int AnimationImporter::FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (unsigned int i = 0; i < pNodeAnim->mNumPositionKeys - 1; i++) 
	{
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) 
		{
			return i;
		}
	}

	assert(0);

	return 0;
}


unsigned int AnimationImporter::FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (unsigned int i = 0; i < pNodeAnim->mNumRotationKeys - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}


unsigned int AnimationImporter::FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (unsigned int i = 0; i < pNodeAnim->mNumScalingKeys - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}

	assert(0);

	return 0;
}

void AnimationImporter::CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 1) {
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	unsigned int PositionIndex = FindPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

void AnimationImporter::CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	// we need at least two values to interpolate...
	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	unsigned int RotationIndex = FindRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}

void AnimationImporter::CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	unsigned int ScalingIndex = FindScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

void AnimationImporter::ReadNodeHierarchy(float AnimationTime, const aiNode* pNode, const Eigen::Matrix4f& ParentTransform)
{
	string NodeName(pNode->mName.C_Str());

	const aiAnimation* pAnimation = m_pScene->mAnimations[0];

	Eigen::Matrix4f NodeTransformation = AiMatrix4x4ToEigenMatrix4f(pNode->mTransformation);

	const aiNodeAnim* pNodeAnim = FindNodeAnim(pAnimation, NodeName);

	if (pNodeAnim) {
		// Interpolate scaling and generate scaling transformation matrix
		aiVector3D Scaling;
		CalcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		Eigen::Matrix4f ScalingM = Eigen::Matrix4f::Zero();

		ScalingM(0, 0) = Scaling.x;
		ScalingM(1, 1) = Scaling.y;
		ScalingM(2, 2) = Scaling.z;
		ScalingM(3, 3) = 1.0f;

		// Interpolate rotation and generate rotation transformation matrix
		aiQuaternion RotationQ;
		CalcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);
		Eigen::Quaternion<Scalar> r = Eigen::Quaternion<Scalar>(RotationQ.w, RotationQ.x, RotationQ.y, RotationQ.z);
		Eigen::Matrix4f RotationM = Eigen::Matrix4f::Identity();
		RotationM.block<3, 3>(0, 0) = r.toRotationMatrix();
		RotationM(3, 3) = 1.0f;

		// Interpolate translation and generate translation transformation matrix
		aiVector3D Translation;
		CalcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		Eigen::Matrix4f TranslationM = Eigen::Matrix4f::Identity();

		TranslationM(0, 3) = Translation.x;
		TranslationM(1, 3) = Translation.y;
		TranslationM(2, 3) = Translation.z;

		// Combine the above transformations
		NodeTransformation = TranslationM * RotationM * ScalingM;
	}

	Eigen::Matrix4f GlobalTransformation = ParentTransform * NodeTransformation;

	if (m_BoneMapping.find(NodeName) != m_BoneMapping.end()) {
		unsigned int BoneIndex = m_BoneMapping[NodeName];

		m_BoneInfo[BoneIndex].FinalTransformation = m_GlobalInverseTransform * GlobalTransformation * m_BoneInfo[BoneIndex].BoneOffset;
	}

	tbb::parallel_for(0, (int)pNode->mNumChildren, [&](size_t i)
	{
		ReadNodeHierarchy(AnimationTime, pNode->mChildren[i], GlobalTransformation);
	});
}

void AnimationImporter::BoneTransform(float TimeInSeconds)
{
	float TicksPerSecond = (float)(m_pScene->mAnimations[0]->mTicksPerSecond != 0 ? m_pScene->mAnimations[0]->mTicksPerSecond : 25.0f);
	float TimeInTicks = TimeInSeconds * TicksPerSecond;
	float AnimationTime = fmod(TimeInTicks, (float)m_pScene->mAnimations[0]->mDuration);

	ReadNodeHierarchy(AnimationTime, m_pScene->mRootNode, Eigen::Matrix4f::Identity());
}

const aiNodeAnim* AnimationImporter::FindNodeAnim(const aiAnimation* pAnimation, const string NodeName)
{
	for (unsigned int i = 0; i < pAnimation->mNumChannels; i++) {
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];

		if (string(pNodeAnim->mNodeName.C_Str()) == NodeName) {
			return pNodeAnim;
		}
	}

	return NULL;
}

Eigen::Matrix4f AnimationImporter::AiMatrix4x4ToEigenMatrix4f(aiMatrix4x4 m)
{
	Eigen::Matrix4f result;

	result(0, 0) = m.a1; result(1, 0) = m.b1; result(2, 0) = m.c1; result(3, 0) = m.d1;
	result(0, 1) = m.a2; result(1, 1) = m.b2; result(2, 1) = m.c2; result(3, 1) = m.d2;
	result(0, 2) = m.a3; result(1, 2) = m.b3; result(2, 2) = m.c3; result(3, 2) = m.d3;
	result(0, 3) = m.a4; result(1, 3) = m.b4; result(2, 3) = m.c4; result(3, 3) = m.d4;

	return result;
}

void AnimationImporter::GetAnimationAtTime(float t, vector<float> & vertices, vector<float> & normals)
{
	if (!m_pScene->HasAnimations()) return;

	BoneTransform(max(t, 0.f));

	tbb::parallel_for(size_t(0), vertices.size() / 3, [&](size_t i)
	{
		Eigen::Matrix4f transform = m_BoneInfo[Bones[i].IDs[0]].FinalTransformation * Bones[i].Weights[0];

		for (int j = 1; j < NUM_BONES_PER_VERTEX; j++)
		{
			transform += m_BoneInfo[Bones[i].IDs[j]].FinalTransformation * Bones[i].Weights[j];
		}

		Eigen::Vector4f v = transform * Eigen::Vector4f(Positions[i].x(), Positions[i].y(), Positions[i].z(), 1.0f);
		Eigen::Vector4f n = transform * Eigen::Vector4f(Normals[i].x(), Normals[i].y(), Normals[i].z(), 0.0f);

		vertices[i * 3 + 0] = v.x();
		vertices[i * 3 + 1] = v.y();
		vertices[i * 3 + 2] = v.z();

		normals[i * 3 + 0] = n.x();
		normals[i * 3 + 1] = n.y();
		normals[i * 3 + 2] = n.z();
	});
}

void AnimationImporter::InterpolateToFirstFrame(float t, vector<float> & vertices, vector<float> & normals)
{
	float time_of_first_frame = 0;

	vector<BoneInfo> start_config = m_BoneInfo;

	BoneTransform(time_of_first_frame);

	vector<BoneInfo> end_config = m_BoneInfo;

	/*for (int i = 0; i < start_config.size(); i++)
	{
		cout << "[" << i << "] Start: " << endl << start_config[i].FinalTransformation << endl << endl << "End: " << endl << end_config[i].FinalTransformation << endl << endl;
	}*/
}