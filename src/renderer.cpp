﻿#include "renderer.h"

#include <iomanip>

//#include "FreeImage.h"

#include <vcg/space/color4.h>
#include <vcg/math/matrix44.h>
#include <vcg/space/deprecated_point4.h>

#include <vcg/space/deprecated_point3.h>
#include <vcg/space/intersection3.h>

#include <chrono>

#include "dynamo_api.h"

void Renderer::glTranslate(const Vector3x & v)
{
	glTranslatef((GLfloat)v[0], (GLfloat)v[1], (GLfloat)v[2]);
}


void Renderer::glRotate(Scalar angle, const Vector3x & v)
{
	glRotatef((GLfloat)angle, (GLfloat)v[0], (GLfloat)v[1], (GLfloat)v[2]);
}


void Renderer::glVertex3(const Vector3x & v)
{
	glVertex3f((GLfloat)v[0], (GLfloat)v[1], (GLfloat)v[2]);
}


Vector3x Renderer::GetPhysicsPos(int pi)
{
	Scalar x, y, z;
	dynamo::get_physics_position(pi, x, y, z);
	return Vector3x(x, y, z);
}

Vector3x Renderer::GetRenderPos(int pi)
{
	Scalar x, y, z;
	dynamo::get_render_position(pi, x, y, z);
	return Vector3x(x, y, z);
}

Vector3x Renderer::GetNor(int pi)
{
	Scalar x, y, z;
	dynamo::get_render_normal(pi, x, y, z);
	return Vector3x(x, y, z);
}

void Renderer::Init(int width, int height)
{
	current_frame = 0;

	// [CUDA Interop]
	//ct = (ComputationType) pVivace->get_computation_type();

	iters = 0;
	quadric = gluNewQuadric();
	gluQuadricOrientation(quadric, GLU_OUTSIDE);
	num_objects = 0;

	UpdateCamera();

	// init shaders
	loadShader("../../demos/shaders/phong.vert",		"../../demos/shaders/phong.frag", phongId);
	loadShader("../../demos/shaders/phong_biased.vert",	"../../demos/shaders/phong.frag", phongBiasedId);
	loadShader("../../demos/shaders/curv.vert",			"../../demos/shaders/curv.frag", curvId);
	loadShader("../../demos/shaders/curv_biased.vert",	"../../demos/shaders/curv.frag", curvBiasedId);
	loadShader("../../demos/shaders/wireframe.vert",	"../../demos/shaders/wireframe.frag", wireframeShaderId);
	loadShader("../../demos/shaders/sphere.vert",		"../../demos/shaders/sphere.frag", sphereShaderId);

	GLfloat light_specular_0[] = { .650f, .650f, .650f, 1.0f };
	GLfloat light_diffuse_0[] = { 1, 1, 1, 1.0 };
	GLfloat light_positio_0[] = { (GLfloat)p_light.x(), (GLfloat)p_light.y(), (GLfloat)p_light.z(), 1 };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse_0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular_0);
	glLightfv(GL_LIGHT0, GL_POSITION, light_positio_0);
	//	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	//	glShadeModel(GL_FLAT);
	glShadeModel(GL_SMOOTH);
	//glEnable(GL_NORMALIZE);

	Reshape(width, height);

	//glEnable(GL_CULL_FACE);
	glDisable(GL_CULL_FACE);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	cloth_bias_loc = glGetUniformLocation(phongBiasedId, "bias");
	same_color_loc = glGetUniformLocation(phongBiasedId, "same_color");
	wireframe_bias_loc = glGetUniformLocation(wireframeShaderId, "wireframe_bias");

	// Override the v-sync?
	glfwSwapInterval(0);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	
	//all_points.reserve(800 * 50 * 3);
	//all_points = new float[800 * 50 * 3];
	
	// [CUDA Interop]
	/*if (ct == ComputationType::GPU_CUDA) {
		createVBO(&vbo, &vbo_col);
	}*/
}

void Renderer::createVBO(GLuint *vbo, GLuint *vbo_col) {

	int num_part = (int)dynamo::get_num_render_vertices();

	// positions + normals (3+3)
	unsigned int vbo_size = num_part * 6 * sizeof(float);

	// rgba (4)
	unsigned int vbo_color_size = num_part * 4 * sizeof(float);

	// POS
	// create buffer object
	glGenBuffers(1, vbo);
	glBindBuffer(GL_ARRAY_BUFFER, *vbo);

	// initialize buffer object
	glBufferData(GL_ARRAY_BUFFER, vbo_size, 0, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// COL
	// create buffer object
	glGenBuffers(1, vbo_col);
	glBindBuffer(GL_ARRAY_BUFFER, *vbo_col);

	// initialize buffer object
	glBufferData(GL_ARRAY_BUFFER, vbo_color_size, 0, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// register this buffer object with CUDA
	// TODO: Implement in api?
	//pVivace->init_VBO(vbo, vbo_col);
}

void Renderer::Shutdown()
{
	//FILE_LOG(logINFO) << "Renderer::Deallocate";

	if (export_to_snapshot)
		ShutdownVideoCapture();

	gluDeleteQuadric(quadric);
}

/*	Adds a mesh to the rendered object list for rendering 
	TODO: Support multiple objects. The indices of the nth object need to have an offset
	according to the sizes of the previous objects
*/
size_t Renderer::AddObject(vector<float> ver, vector<float> nor, vector<int> idx, bool wireframe)
{
	/*if (num_objects > 0)
	{
		for (int i = 0; i < idx.size(); i++)
		{
			idx[i] += render_vertices.size();
		}		
	}

	object_offsets.push_back(render_vertices.size());*/

	/*render_vertices.insert(render_vertices.end(), ver.begin(), ver.end());
	render_normals.insert(render_normals.end(), nor.begin(), nor.end());
	render_indices.insert(render_indices.end(), idx.begin(), idx.end());*/

	render_vertices = ver;
	render_normals = nor;
	render_indices = idx;
	 
	object_wireframe = wireframe;

	return num_objects++;
}

void Renderer::UpdateObject(int id, vector<float> ver, vector<float> nor)
{
	render_vertices = ver;
	render_normals = nor;

	/*memcpy(render_vertices.data() + 0, ver.data(), ver.size() * sizeof(float));
	memcpy(render_normals.data() + 0, nor.data(), nor.size() * sizeof(float));*/
}

void Renderer::setupPerspective(int width, int height)
{
	// Determine the new aspect ratio
	GLdouble gldAspect = (GLdouble)width / (GLdouble)height;

	// Reset the projection matrix with the new aspect ratio.
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(camera_fov, gldAspect, .01, 400);
}

void Renderer::Reshape(int width, int height)
{
	this->width = width;
	this->height = height;

	setupPerspective(width, height);

	// Set the viewport to take up the entire window.
	glViewport(0, 0, width, height);
}


void Renderer::Draw(int width, int height)
{
	

	glUseProgramObjectARB(0);

	GLfloat light_positio_0[] = { (GLfloat)p_light.x(), (GLfloat)p_light.y(), (GLfloat)p_light.z(), 1 };
	glLightfv(GL_LIGHT0, GL_POSITION, light_positio_0);

	setupMatrices(p_camera, u_camera, l_camera);

	

	// draw light
	if (show_light)
	{
		glUseProgramObjectARB(0);
		glDisable(GL_LIGHTING);
		glPushMatrix();
		glTranslate(p_light);
		gluSphere(quadric, .25f, 20, 20);
		glPopMatrix();
		glEnable(GL_LIGHTING);
	}
	//glCullFace(GL_BACK);

	glViewport(0, 0, width, height);

	//Enabling color write (previously disabled for light POV z-buffer rendering)
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	// Clear previous frame values

	//glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glUseProgram(phongId);

	if (show_grid)
	{
		glColor3f(0.75f, 0.75f, 0.75f);
		DrawGrid();
	}

	glEnable(GL_CULL_FACE);
	DrawColliders();
	glDisable(GL_CULL_FACE);

	// Draw the rendered objects that the user
	// has added to the scene
	glUseProgram(phongId);
	if (render_vertices.size() > 0) {
		if (object_wireframe) {
			glUseProgramObjectARB(wireframeShaderId);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			DrawObjects();
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
		else {
			DrawObjects();
		}
		//DrawObjects();

		

		/*if (wireframe_enabled)
		{
			glUseProgramObjectARB(wireframeShaderId);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			DrawObjects();
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}*/
	}
	
	
	glUseProgram(phongBiasedId);
	
	if (cloth_bias_loc != -1)
	{
		glUniform1f(cloth_bias_loc, cloth_bias);
	}

	if (same_color_loc != -1)
	{
		glUniform1f(same_color_loc, (same_color ? 1.0f : 0.0f));
	}

	//glUseProgram(phongId);
	DrawParticleSystem();
	if (wireframe_enabled)
	{
		glEnable(GL_BLEND);

		glUseProgramObjectARB(wireframeShaderId);
		if (wireframe_bias_loc != -1)
		{
			glUniform1f(wireframe_bias_loc, wireframe_bias);
		}

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		DrawParticleSystem();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		
		glDisable(GL_BLEND);
	}

	if (show_particles)
		DrawParticles();// draw just  the particles

	// draw fixed particles
	if (0)
	{
		glUseProgram(phongId);
		glColor3f(0.f, 1.f, 0.f);

		size_t pos_num = dynamo::get_num_render_vertices();
		for (auto pi = 0; pi < pos_num; pi++)
		{
			if (dynamo::get_invmass(pi) == 0)
			{
				Vector3x v = GetRenderPos(pi);
				glPushMatrix();
				glTranslate(v);
				gluSphere(quadric, .09, 10, 10);
				glPopMatrix();
			}
		}
	}

	if (show_axis)
		DrawAxes(16);

	glUseProgramObjectARB(0);
	current_frame++;
}

//void Renderer::DrawParticles(Vivace & vivace)
void Renderer::DrawParticles()
{
	glUseProgram(phongId);
	glColor3f(0.91f, 0.14f, 0.15f);//213, 62, 79
/*
		// cpu or cpu_tbb
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glVertexPointer(3, GL_DOUBLE, sizeof(Vector3x), &(vivace.pos(0)[0]));
	glColorPointer(3, GL_DOUBLE, sizeof(Vector3x), &(vivace.color_part(0)[0]));
	glDrawArrays(GL_POINTS, 0, (GLsizei)vivace.pos_num());
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
*/
	glUseProgramObjectARB(0);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_POINT_SPRITE_ARB);
}

void Renderer::DrawParticles(vector<unsigned int> & indices)
{
	if (indices.empty())
		return;

	glUseProgram(phongId);
	glColor3f(0.91f, 0.91f, 0.15f);


	for (auto i = 0; i < indices.size(); i++)
	{
		Vector3x v = GetRenderPos(indices[i]);

		glPushMatrix();
		glTranslate(v);
		gluSphere(quadric, .1, 10, 10);
		glPopMatrix();
	}

	glUseProgram(0);
}


void Renderer::Finalize()
{
	if ((iters == 0) && (export_to_snapshot))
		InitVideoCapture();

	if (export_to_snapshot)
		Snapshot();
	//UpdateVideoCapture();
//Snapshot();

//if ((iters % 2) == 0)
//	glutSwapBuffers();
	iters++;
}

// video capture
void Renderer::InitVideoCapture()
{
	// http://blog.mmacklin.com/2013/06/11/real-time-video-capture-with-ffmpeg/
	// start ffmpeg telling it to expect raw rgba 720p-60hz frames
	// -i - tells it to read frames from stdin
	const char* cmd = "ffmpeg -r 60 -f rawvideo -pix_fmt rgba -s 1280x720 -i - "
		"-threads 0 -preset fast -y -pix_fmt yuv420p -crf 21 -vf vflip output.mp4";

	// open pipe to ffmpeg's stdin in binary write mode
	ffmpeg = _popen(cmd, "wb");
	pixels = new int[width * height];
}

void Renderer::UpdateVideoCapture()
{
	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	fwrite(pixels, sizeof(int)* width * height, 1, ffmpeg);
}

void Renderer::ShutdownVideoCapture()
{
	delete[] pixels;
	_pclose(ffmpeg);
}


// each element of the array (color, RGBA) is formed by 4 bytes, the last one is FF because the color is always fully opaque
void Renderer::Snapshot(BYTE* array, unsigned int width, unsigned int height, const string & filename, bool topdown)
{
#if 0
	// Convert to FreeImage format & save to file
	FIBITMAP* img_0 = FreeImage_ConvertFromRawBits(array, width, height, 4 * width, 32, 0x000000FF, 0x00000000, 0x0000000, topdown);
	FIBITMAP* img_1 = FreeImage_ConvertTo24Bits(img_0);
	FreeImage_Save(FIF_PNG, img_1, filename.c_str(), 0);

	// Free resources
	FreeImage_Unload(img_0);
	FreeImage_Unload(img_1);
#endif
}


void Renderer::Snapshot()
{
#if 0
	// to convert to a video use
	// ffmpeg -f image2 -i frame_%06d.png video.mp4

	std::ostringstream ss;
	ss << std::setw(6) << std::setfill('0') << iters;
	const std::string s = ss.str();

	string filename = string("./frames_gl/frame_") + s + string(".png");

	// Make the BYTE array, factor of 3 because it's RBG.
	BYTE* pixels = new BYTE[4 * width * height];

	glReadPixels(0, 0, width, height, GL_BGRA, GL_UNSIGNED_BYTE, pixels);

	// Convert to FreeImage format & save to file
	FIBITMAP* img_0 = FreeImage_ConvertFromRawBits(pixels, width, height, 4 * width, 32, 0x000000FF, 0x00000000, 0x0000000, false);
	FIBITMAP* img_1 = FreeImage_ConvertTo24Bits(img_0);
	FreeImage_Save(FIF_PNG, img_1, filename.c_str(), 0);

	// Free resources
	FreeImage_Unload(img_0);
	FreeImage_Unload(img_1);
	delete[] pixels;
#endif
}

// draw the coordinate axes
void Renderer::DrawAxes(float length)
{
	//	glUseProgramObjectARB(wireframeShaderId);
	glUseProgramObjectARB(0);
	glEnable(GL_BLEND);	
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glPushMatrix();
	glScalef(length, length, length);

	glLineWidth(4.f);
	glBegin(GL_LINES);

	// x red
	glColor4f(1.f, 0.f, 0.f, .25f);
	glVertex3f(0.f, 0.f, 0.f);
	glVertex3f(1.f, 0.f, 0.f);

	// y green
	glColor4f(0.f, 1.f, 0.f, .25f);
	glVertex3f(0.f, 0.f, 0.f);
	glVertex3f(0.f, 1.f, 0.f);

	// z blue
	glColor4f(0.f, 0.f, 1.f, .25f);
	glVertex3f(0.f, 0.f, 0.f);
	glVertex3f(0.f, 0.f, 1.f);

	glEnd();

	glPopMatrix();

	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glLineWidth(1.f);
	glUseProgramObjectARB(0);

}


void Renderer::DrawColliders()
{
	for (int id = 0; id < (int)dynamo::get_num_colliders(); id++)
	{
		string type(dynamo::get_collider_type(id));

		// TODO: Reintroduce SDF collider?
		//if (type == "Signed Distance Field")
		//{
		//	// very inefficient... but it works for the sake of debugging
		//	glUseProgramObjectARB(wireframeShaderId);
		//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		//
		//	float * pvert;
		//	int * pface;
		//	size_t num_face_idx;
		//	
		//	//pVivace->get_coll_sdf(id, &pvert, &pface, num_face_idx);
		//
		//	glBegin(GL_TRIANGLES);
		//	for (size_t i = 0; i < num_face_idx / 3; i++)
		//	{
		//		for (size_t j = 0; j < 3; j++)
		//		{
		//			int index = pface[i * 3 + j];
		//			glVertex3f(
		//				pvert[index * 3 + 0],
		//				pvert[index * 3 + 1],
		//				pvert[index * 3 + 2]);
		//		}
		//	}
		//	glEnd();
		//	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		//}

		if (type == "Sphere")
		{
			Scalar radius = 0;
			Vector3x pos(0, 0, 0);
			dynamo::get_sphere_collider(id, radius, pos[0], pos[1], pos[2]);
			glPushMatrix();
			glTranslate(pos);
			/*glColor3f(0.f, 0.5f, 0.8f);*/
			glColor3f(1.0f, 1.0f, 1.0f);
			gluSphere(quadric, radius * 0.95, 60, 60);
			glPopMatrix();
		}

		if (type == "Plane")
		{
			Vector3x pos(0, 0, 0);
			Vector3x nor(0, 0, 0);
			dynamo::get_plane_collider(id, pos[0], pos[1], pos[2], nor[0], nor[1], nor[2]);
			glPushMatrix();
			glTranslate(pos - nor * 0.02f);
			glColor3f(0.65f, 0.65f, 0.75f);
			float angle_rad = (float)acos(Vector3x(0, 1, 0).dot(nor));
			if (angle_rad != 0)
			{
				Vector3x axis = (Vector3x(0, 1, 0).cross(nor));
				glRotate((angle_rad / 3.14f) * 180, axis);
			}
			GLfloat size = 500.f;
			glBegin(GL_QUADS);
			glNormal3f(0, 1, 0);
			glVertex3f(-size, 0, -size);
			glVertex3f(-size, 0, size);
			glVertex3f(size, 0, size);
			glVertex3f(size, 0, -size);
			glEnd();
			glPopMatrix();
		}

		if (type == "Capsule")
		{
			Scalar radius0 = 0;
			Scalar radius1 = 0;
			Vector3x p0(0, 0, 0);
			Vector3x p1(0, 0, 0);

			dynamo::get_capsule_collider(id, radius0, radius1, p0[0], p0[1], p0[2], p1[0], p1[1], p1[2]);
			radius0 *= 0.95f;
			radius1 *= 0.95f;

			Scalar h = (p1 - p0).norm();
			Vector3x dir(p1 - p0);
			dir.normalize();

			glPushMatrix();
			glColor3f(0.f, 0.5f, 0.8f);

			glTranslate(p0);

			float angle_rad = (float)acos(Vector3x(0, 1, 0).dot(dir));
			if (angle_rad != 0)
			{
				Vector3x axis = (Vector3x(0, 1, 0).cross(dir));
				axis.normalize();
				glRotate((angle_rad / 3.14f) * 180.f, axis);
			}
			glRotatef(-90.f, 1.f, 0.f, 0.f);
			//gluDisk(gluNewQuadric(), 0, radius, 20, 20);
			gluSphere(gluNewQuadric(), radius0, 20, 20);

			gluCylinder(gluNewQuadric(), radius0, radius1, h, 20, 20);
			glTranslate(Vector3x(0, 0, h));
			//gluDisk(gluNewQuadric(), 0, radius, 20, 20);
			gluSphere(gluNewQuadric(), radius1, 20, 20);

			glPopMatrix();

		}

		if (type == "Box")
		{
			Vector3x box_min(0, 0, 0);
			Vector3x box_max(0, 0, 0);
			dynamo::get_axis_aligned_box_collider(id, box_min[0], box_min[1], box_min[2], box_max[0], box_max[1], box_max[2]);
			glPushMatrix();

			// TODO: Wrong bias
			glScalef(0.99f, 0.99f, 0.99f);
			DrawBox(box_min, box_max);
			glPopMatrix();
		}

		if (type == "Oriented Bounding Box")
		{
			Vector3x c;
			Vector3x u[3];
			Vector3x e;

			dynamo::get_object_oriented_box_collider(id,
				c[0], c[1], c[2],
				u[0][0], u[0][1], u[0][2],
				u[1][0], u[1][1], u[1][2],
				u[2][0], u[2][1], u[2][2],
				e[0], e[1], e[2]);

			glPushMatrix();
			for (auto i = 0; i < 3; i++)	e[i] *= .96f;
//			glScalef(0.96f, 0.96f, 0.96f);
			DrawBox(c, u, e);
			glPopMatrix();
		}


		if (type == "Mesh Collider" && !hide_mesh_collider) {
			// very inefficient... but it works for the sake of debugging



			glUseProgramObjectARB(wireframeShaderId);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			Scalar * pvert;
			int * pface;
			unsigned int num_face_idx;
			unsigned int num_vert;
			dynamo::get_mesh_collider(id, &pvert, num_vert, &pface, num_face_idx);

			glBegin(GL_TRIANGLES);
			for (size_t i = 0; i < num_face_idx / 3; i++)
			{
				for (size_t j = 0; j < 3; j++)
				{
					int index = pface[i * 3 + j];
					glVertex3f(
						pvert[index * 3 + 0],
						pvert[index * 3 + 1],
						pvert[index * 3 + 2]);
				}
			}

			glEnd();
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}

		if (type == "Self" && show_surface_particles)
		{
			glUseProgram(phongId);

			std::vector<Vector3x> surface_particles;
			float radius = 0;

			//pVivace->get_surface_particles(id, surface_particles, radius);

			for (int i = 0; i < surface_particles.size(); i++)
			{
				glPushMatrix();
				glColor3f(0.1f, 0.9f, 0.9f);
				glTranslate(surface_particles[i]);
				gluSphere(quadric, radius, 10, 10);
				glPopMatrix();
			}

			glUseProgram(0);
		}
	}

	/*

		if (is_collision_enabled_cyl == 2)
		{
			glPushMatrix();

			glColor3f(0.f, 0.5f, 0.8f);

			static float radius = ConfigHandler::ReadFloat("Cylinder.radius") * 0.975f;
			static float omega = ConfigHandler::ReadFloat("Cylinder.omega");
			static Vector3x p2 = ConfigHandler::ReadVector3x("Cylinder.p1");;
			static Vector3x p1 = ConfigHandler::ReadVector3x("Cylinder.p0");
			glBegin(GL_QUADS);
			for (float x = p1.x(); x < p2.x(); x += 0.1)
			{
				for (float ir = 0; ir < 2 * 3.14159265359; ir += 0.1)
				{
					Vector3x pos, normal;
					SinCylCoord(x, ir, omega, radius, pos, normal);
					SinCylCoord(x + 0.1, ir, omega, radius, pos, normal);
					SinCylCoord(x + 0.1, ir + 0.1, omega, radius, pos, normal);
					SinCylCoord(x, ir + 0.1, omega, radius, pos, normal);
				}
			}
			glEnd();
			glPopMatrix();
		}
	*/
}


void Renderer::DrawMesh(Scalar * vert, unsigned int vert_num, int * tri, unsigned int idx_num)
{
	glUseProgramObjectARB(wireframeShaderId);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	// draw the mesh
	glEnableClientState(GL_VERTEX_ARRAY);

	if (sizeof(Scalar) == sizeof(double))
		glVertexPointer(3, GL_DOUBLE, sizeof(Scalar) * 3, vert);

	if (sizeof(Scalar) == sizeof(float))
		glVertexPointer(3, GL_FLOAT, sizeof(Scalar) * 3, vert);

	glDrawElements(GL_TRIANGLES, (GLsizei)idx_num, GL_INT, tri);

	glDisableClientState(GL_VERTEX_ARRAY);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Renderer::Reset() {
	render_indices.clear();
	render_vertices.clear();
	render_normals.clear();



	hide_mesh_collider = false;
}

void Renderer::DrawObjects()
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glColor3f(0.8f, 0.8f, 0.8f);

	if (sizeof(Scalar) == sizeof(double))
	{
		glVertexPointer(3, GL_DOUBLE, sizeof(Vector3x), render_vertices.data());
		glNormalPointer(GL_DOUBLE, sizeof(Vector3x),    render_normals.data());
	}
	if (sizeof(Scalar) == sizeof(float))
	{
		glVertexPointer(3, GL_FLOAT, sizeof(Vector3x), render_vertices.data());
		glNormalPointer(GL_FLOAT, sizeof(Vector3x), render_normals.data());

		glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(render_indices.size()), GL_UNSIGNED_INT, render_indices.data());		
	}

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}


// draw the particle system
void Renderer::DrawParticleSystem()
{
	if (dynamo::get_num_render_vertices() == 0)
		return;

	if (dynamo::get_num_render_indices() == 0)
		return;

	// draw the mesh
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	if (sizeof(Scalar) == sizeof(double))
	{
		glVertexPointer(3, GL_DOUBLE, sizeof(Vector3x), dynamo::get_render_vertex_pointer());
		glColorPointer(3, GL_DOUBLE, sizeof(Vector3x), dynamo::get_render_color_pointer());
		glNormalPointer(GL_DOUBLE, sizeof(Vector3x), dynamo::get_render_normal_pointer());
	}
	if (sizeof(Scalar) == sizeof(float))
	{
		//// [CUDA Interop]
		//if (ct == GPU_CUDA) {
		//	glBindBuffer(GL_ARRAY_BUFFER, vbo);
		//	glVertexPointer(3, GL_FLOAT, 6 * sizeof(float), 0);
		//	glNormalPointer(GL_FLOAT, 6 * sizeof(float), (const GLvoid*)(3 * sizeof(float)));
		//	glBindBuffer(GL_ARRAY_BUFFER, vbo_col);
		//	glColorPointer(4, GL_FLOAT, 0, 0);
		//}
		//else {
		//	glVertexPointer(3, GL_FLOAT, sizeof(Vector3x), pVivace->get_render_pos_pointer());
		//	glColorPointer(3, GL_FLOAT, sizeof(Vector3x), pVivace->get_render_col_pointer());
		//	glNormalPointer(GL_FLOAT, sizeof(Vector3x), pVivace->get_render_nor_pointer());
		//}

		glVertexPointer(3, GL_FLOAT, sizeof(Vector3x), dynamo::get_render_vertex_pointer());
		glColorPointer(3, GL_FLOAT, sizeof(Vector3x), dynamo::get_render_color_pointer());
		glNormalPointer(GL_FLOAT, sizeof(Vector3x), dynamo::get_render_normal_pointer());

		const size_t num_objs = dynamo::get_num_deformable_objects();
		for (auto id = 0; id < num_objs; id++)
		{
			if (!dynamo::is_enabled(id))		continue;

			glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(dynamo::get_num_render_faces_object(id)), GL_UNSIGNED_INT, dynamo::get_render_face_pointer_object(id));

		}
		// [CUDA Interop]
		//glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	// draw normals
	if (0)//(show_normals)
	{
		glColor3f(0.f, 0.f, 1.0f);
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
		size_t pos_num = dynamo::get_num_render_vertices();
		for (auto i = 0; i < pos_num; i++)
		{
			const Vector3x & a = GetRenderPos(i);
			const Vector3x & b = a + GetNor(i);

			glVertex3(a);
			glVertex3(b);
		}
		glEnd();
	}
}

void Renderer::DrawGrid()
{
	glPushMatrix();

	glBegin(GL_QUADS);
	glNormal3f(0, 1, 0);
	glVertex3f(-size_grid, 0, -size_grid);
	glVertex3f(-size_grid, 0, size_grid);
	glVertex3f(size_grid, 0, size_grid);
	glVertex3f(size_grid, 0, -size_grid);
	glEnd();


	//glColor3f(0.f, 0.f, 0.f);
	//glBegin(GL_LINES);
	//for (int i = -100; i <= 100; i += 1)
	//{
	//	glVertex3f((float)i, 0.01f, -100.0f);
	//	glVertex3f((float)i, 0.01f, 100.0f);
	//	glVertex3f(-100.0f, 0.01f, (float)i);
	//	glVertex3f(100.0f, 0.01f, (float)i);
	//}
	//glEnd();

	glPopMatrix();
}



// draw aabox
// before calling, use 			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); or 			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

void Renderer::DrawBox(Vector3x min, Vector3x max)
{
	glColor3f(0.75f, 0.75f, 0.85f);

	Vector3x eps = Vector3x(0.0001f, 0.0001f, 0.0001f);
	min = min + eps;
	max = max - eps;

	Vector3x v0(min[0], min[1], min[2]);
	Vector3x v1(max[0], min[1], min[2]);
	Vector3x v2(max[0], min[1], max[2]);
	Vector3x v3(min[0], min[1], max[2]);

	Vector3x v4(min[0], max[1], min[2]);
	Vector3x v5(max[0], max[1], min[2]);
	Vector3x v6(max[0], max[1], max[2]);
	Vector3x v7(min[0], max[1], max[2]);

	glBegin(GL_QUADS);
	glNormal3f(0.f, -1.f, 0.f);
	glVertex3(v0);
	glVertex3(v1);
	glVertex3(v2);
	glVertex3(v3);

	glNormal3f(0.f, 1.f, 0.f);
	glVertex3(v7);
	glVertex3(v6);
	glVertex3(v5);
	glVertex3(v4);

	glNormal3f(1.f, 0.f, 0.f);
	glVertex3(v5);
	glVertex3(v6);
	glVertex3(v2);
	glVertex3(v1);

	glNormal3f(-1.f, 0.f, 0.f);
	glVertex3(v3);
	glVertex3(v7);
	glVertex3(v4);
	glVertex3(v0);

	glNormal3f(0.f, 0.f, 1.f);
	glVertex3(v3);
	glVertex3(v2);
	glVertex3(v6);
	glVertex3(v7);

	glNormal3f(0.f, 0.f, -1.f);
	glVertex3(v4);
	glVertex3(v5);
	glVertex3(v1);
	glVertex3(v0);


	glEnd();
}

void Renderer::DrawBox(Vector3x c, Vector3x u[3], Vector3x e)
{
	glColor3f(0.75f, 0.75f, 0.85f);

	//Vector3x eps = Vector3x(0.0001f, 0.0001f, 0.0001f);
	//min = min + eps;
	//max = max - eps;

	Vector3x v0 = c - e[0] * u[0] - e[1] * u[1] - e[2] * u[2];
	Vector3x v1 = c + e[0] * u[0] - e[1] * u[1] - e[2] * u[2];
	Vector3x v2 = c + e[0] * u[0] - e[1] * u[1] + e[2] * u[2];
	Vector3x v3 = c - e[0] * u[0] - e[1] * u[1] + e[2] * u[2];

	Vector3x v4 = c - e[0] * u[0] + e[1] * u[1] - e[2] * u[2];//(min[0], max[1], min[2]);
	Vector3x v5 = c + e[0] * u[0] + e[1] * u[1] - e[2] * u[2];//(max[0], max[1], min[2]);
	Vector3x v6 = c + e[0] * u[0] + e[1] * u[1] + e[2] * u[2];//(max[0], max[1], max[2]);
	Vector3x v7 = c - e[0] * u[0] + e[1] * u[1] + e[2] * u[2];//(min[0], max[1], max[2]);

	glBegin(GL_QUADS);
	glNormal3f(0.f, -1.f, 0.f);
	glVertex3(v0);
	glVertex3(v1);
	glVertex3(v2);
	glVertex3(v3);

	glNormal3f(0.f, 1.f, 0.f);
	glVertex3(v7);
	glVertex3(v6);
	glVertex3(v5);
	glVertex3(v4);

	glNormal3f(1.f, 0.f, 0.f);
	glVertex3(v5);
	glVertex3(v6);
	glVertex3(v2);
	glVertex3(v1);

	glNormal3f(-1.f, 0.f, 0.f);
	glVertex3(v3);
	glVertex3(v7);
	glVertex3(v4);
	glVertex3(v0);

	glNormal3f(0.f, 0.f, 1.f);
	glVertex3(v3);
	glVertex3(v2);
	glVertex3(v6);
	glVertex3(v7);

	glNormal3f(0.f, 0.f, -1.f);
	glVertex3(v4);
	glVertex3(v5);
	glVertex3(v1);
	glVertex3(v0);


	glEnd();
}



void Renderer::loadShader(char* vshader, char* fshader, GLhandleARB & id)
{
	GLhandleARB vertexShaderHandle = loadShaderCode(vshader, GL_VERTEX_SHADER);
	GLhandleARB fragmentShaderHandle = loadShaderCode(fshader, GL_FRAGMENT_SHADER);

	id = glCreateProgramObjectARB();

	glAttachObjectARB(id, vertexShaderHandle);
	glAttachObjectARB(id, fragmentShaderHandle);
	glLinkProgramARB(id);
}

// Loading shader function
GLhandleARB Renderer::loadShaderCode(char* filename, unsigned int type)
{
	FILE *pfile;
	GLhandleARB handle;
	const GLcharARB* files[1];

	// shader Compilation variable
	GLint result;				// Compilation code result
	GLint errorLoglength;
	char* errorLogText;
	GLsizei actualErrorLogLength;

	char buffer[400000];
	memset(buffer, 0, 400000);

	// This will raise a warning on MS compiler
	fopen_s(&pfile, filename, "rb");
	if (!pfile)
	{
		printf("Sorry, can't open file: '%s'.\n", filename);
		exit(0);
	}

	fread(buffer, sizeof(char), 400000, pfile);
	//printf("%s\n",buffer);


	fclose(pfile);

	handle = glCreateShaderObjectARB(type);
	if (!handle)
	{
		//We have failed creating the vertex shader object.
		printf("Failed creating vertex shader object from file: %s.", filename);
		exit(0);
	}

	files[0] = (const GLcharARB*)buffer;
	glShaderSourceARB(
		handle, //The handle to our shader
		1, //The number of files.
		files, //An array of const char * data, which represents the source code of theshaders
		NULL);

	glCompileShaderARB(handle);

	//Compilation checking.
	glGetObjectParameterivARB(handle, GL_OBJECT_COMPILE_STATUS_ARB, &result);

	// If an error was detected.
	if (!result)
	{
		//We failed to compile.
		printf("Shader '%s' failed compilation.\n", filename);

		//Attempt to get the length of our error log.
		glGetObjectParameterivARB(handle, GL_OBJECT_INFO_LOG_LENGTH_ARB, &errorLoglength);

		//Create a buffer to read compilation error message
		errorLogText = (char*)malloc(sizeof(char)* errorLoglength);

		//Used to get the final length of the log.
		glGetInfoLogARB(handle, errorLoglength, &actualErrorLogLength, errorLogText);

		// Display errors.
		printf("%s\n", errorLogText);

		// Free the buffer malloced earlier
		free(errorLogText);
	}

	return handle;
}

void Renderer::setupMatrices(Vector3x position, Vector3x up, Vector3x lookAt)
{
	setupPerspective(width, height);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(
		position.x(), position.y(), position.z(),
		lookAt.x(), lookAt.y(), lookAt.z(),
		up.x(), up.y(), up.z());

	glRotatef(camera_azimuth, VectorUp.x(), VectorUp.y(), VectorUp.z());	// cam_azimuth is modified by the mouse
}


void Renderer::UpdateCamera()
{
	Vector3x dir_camera((p_camera - l_camera) * -1);
	dir_camera.normalize();
	u_camera = dir_camera.cross(r_camera);
	u_camera.normalize();
	r_camera = u_camera.cross(dir_camera);
	r_camera.normalize();

}

// todo(marco): obsolete -- remove
void Renderer::PrintCamera()
{
	printf("CAMERA:\n");
	printf("<Pos>%.3f,%.3f,%.3f</Pos>\n", p_camera.x(), p_camera.y(), p_camera.z());
	printf("<Up>%.3f,%.3f,%.3f</Up>\n", u_camera.x(), u_camera.y(), u_camera.z());
	printf("<Right>%.3f,%.3f,%.3f</Right>\n", r_camera.x(), r_camera.y(), r_camera.z());
	printf("<LookAt>%.3f,%.3f,%.3f</LookAt>\n", l_camera.x(), l_camera.y(), l_camera.z());
	printf("<Azimuth>%.3f</Azimuth>\n", camera_azimuth);
}


void Renderer::Get_Selection_Ray(int mouse_x, int mouse_y, double* p, double* q)
{
	// Convert (x, y) into the 2D unit space
	double new_x = (double)(2 * mouse_x) / (double)width - 1;
	double new_y = 1 - (double)(2 * mouse_y) / (double)height;


	// Convert (x, y) into the 3D viewing space
	double P[16], M[16];
	int V[4];
	glGetIntegerv(GL_VIEWPORT, V);
	glGetDoublev(GL_MODELVIEW_MATRIX, M);
	glGetDoublev(GL_PROJECTION_MATRIX, P);

	//	double y = double(V[3] - mouse_y);

	gluUnProject(mouse_x, mouse_y, 0.0, M, P, V, &(p[0]), &(p[1]), &(p[2]));
	gluUnProject(mouse_x, mouse_y, 1.0, M, P, V, &(q[0]), &(q[1]), &(q[2]));

	//printf("p: %.3f %.3f %.3f\n", p[0], p[1], p[2]);
	//printf("q: %.3f %.3f %.3f\n", q[0], q[1], q[2]);
}


void Renderer::Select(double _p[], double _q[], int & selected_pi, double & ds)
{
	vcg::Point3d p(_p[0], _p[1], _p[2]);
	vcg::Point3d q(_q[0], _q[1], _q[2]);
	vcg::Point3d dir(q - p);

	dir.Normalize();

	double min_t = MY_INFINITE;
	int	 select_f;
	double t, u, v;

	for (int f = 0; f < (int)dynamo::get_num_physics_indices(); f += 3)
	{
		t = MY_INFINITE;
		Vector3x pos0 = GetPhysicsPos(dynamo::get_particle_index_from_face(f + 0));
		Vector3x pos1 = GetPhysicsPos(dynamo::get_particle_index_from_face(f + 1));
		Vector3x pos2 = GetPhysicsPos(dynamo::get_particle_index_from_face(f + 2));
		vcg::Point3d  p0 = vcg::Point3d(pos0[0], pos0[1], pos0[2]);
		vcg::Point3d  p1 = vcg::Point3d(pos1[0], pos1[1], pos1[2]);
		vcg::Point3d  p2 = vcg::Point3d(pos2[0], pos2[1], pos2[2]);

		vcg::Ray3d ray(p, dir);

		if (vcg::IntersectionRayTriangle(ray, p0, p1, p2, t, u, v))
		{
			//printf("hit!\n");
			if (t < min_t)
			{
				select_f = f;
				min_t = t;
			}
		}
	}

	selected_pi = -1;
	if (min_t != MY_INFINITE)	//Selection made
	{
		double w = 1 - (u + v);
		//printf("u: %.3f v: %.3f w: %.3f\n", u, v, w);
		if (u < v && u < w)		selected_pi = (int)dynamo::get_particle_index_from_face(select_f + 0);
		else if (v < w)			selected_pi = (int)dynamo::get_particle_index_from_face(select_f + 1);
		else					selected_pi = (int)dynamo::get_particle_index_from_face(select_f + 2);

		assert(selected_pi != -1);
		//printf("select_v: %d\n", selected_pi);
		double P[16], M[16];
		int V[4];
		glGetIntegerv(GL_VIEWPORT, V);
		glGetDoublev(GL_MODELVIEW_MATRIX, M);
		glGetDoublev(GL_PROJECTION_MATRIX, P);

		vcg::Point3d obj = p + dir * min_t;
		double a, b;
		gluProject(obj[0], obj[1], obj[2], M, P, V, &a, &b, &ds);
	}

	//printf("select_f: %d\n", select_f);
}


