#include "app.h"

#include <gl/glew.h>
#include <GLFW/glfw3.h>

#include "timer.h"
#include "cereal/cereal.hpp"
#include "cereal/archives/xml.hpp"

#include <fstream>

#include "vcg\math\quaternion.h"

#include "dynamo_api.h"

#include "demos/hanging_cloth.h"
#include "demos/implicit_colliders.h"
#include "demos/ribbon.h"
#include "demos/collision_mesh_skinned_remeshed.h"
#include "demos/collision_box.h"
#include "demos/collision_obbox.h"
#include "demos/stitching.h"
#include "demos/collision_moving_capsule.h"
#include "demos/anchor_to_sphere.h"
#include "demos/load_tetra_mesh.h"
#include "demos/friction_sphere.h"
#include "demos/stacking.h"
#include "demos/characterf_backflip_asymmetric.h"
#include "demos/characterf_samba_berkeley_dress.h"
#include "demos/characterf_run_in_circle.h"
#include "demos/characterf_roundhouse_tshirt.h"

#include "tiny_obj_loader.h"

// used for the trackball implementation
const double m_ROTSCALE = 0.1;
const double m_ZOOMSCALE = 0.008;

double sim_time = 0;
double draw_time = 0;

size_t id_sphere = -1;


const float CAMERA_AZIMUTH = -179.800f;
const float CAMERA_FOV = 45;
const Vector3x P_CAMERA = Vector3x(0, 6.4f, -9.1f);				// Camera position	
const Vector3x U_CAMERA = Vector3x(-0.012f, 0.818f, 0.575f);	// Camera up	
const Vector3x R_CAMERA = Vector3x(1, 0, 0);					// Camera right	
const Vector3x L_CAMERA = Vector3x(0, 0, 0);					// Camera lookAt

static void error_callback(int error, const char* description)
{
	//FILE_LOG(logINFO) << "Error " << error << ": " << description;
}

vcg::Point3f FromVector3x(const Vector3x & v)
{
	return vcg::Point3f(float(v[0]), float(v[1]), float(v[2]));
}


template <class Archive>
void DynamoApp::serialize(Archive & archive)
{
	//	archive(cereal::make_nvp("log_reporting_level", FILELog::ReportingLevel()));
	archive(cereal::make_nvp("window_width", win_width));
	archive(cereal::make_nvp("window_height", win_height));

	archive(cereal::make_nvp("start_freeze", bFreeze));
	archive(CEREAL_NVP(end_frame));

	archive(CEREAL_NVP(renderer));
}


void DynamoApp::Load()
{
	// load all the parameters needed to initialize
	std::ifstream file("../../demos/config/in.xml");
	cereal::XMLInputArchive archive(file);

	serialize(archive);
}

void DynamoApp::Save()
{
	// save all the parameters needed to initialize
	std::ofstream file("../../demos/config/client.xml");
	cereal::XMLOutputArchive archive(file, cereal::XMLOutputArchive::Options(5, true, false));
	serialize(archive);
}

DynamoApp::DynamoApp()
{
	demos.push_back(new DemoHangingCloth());
	demos.push_back(new DemoImplicitColliders());
	demos.push_back(new DemoRibbon());
	demos.push_back(new DemoCollisionBox());
	demos.push_back(new DemoCollisionOBBox());
	demos.push_back(new DemoStitching());
	demos.push_back(new DemoCollisionMovingCapsule());
	demos.push_back(new DemoAnchorToSphere());
	demos.push_back(new DemoLoadTetraMesh());
	demos.push_back(new DemoFrictionSphere());
	demos.push_back(new DemoStacking());
	demos.push_back(new DemoCollisionMeshSkinnedRemeshed());
	demos.push_back(new DemoCharacterFBackflipAsymmetric());
	demos.push_back(new DemoCharacterFSambaBerkeleyDress());
	demos.push_back(new DemoCharacterFRunInCircle());
	demos.push_back(new DemoCharacterFRoundhouseTshirt());

	cout << "available demos:" << endl;
	for (const auto & demo : demos)
		cout << demo->Name() << endl;
	
	currDemo = 0;
}

void DynamoApp::Init(bool init_gl)
{
	cur_frame = 0;
	isDragging = false;
	selectingFixed = false;
	pickOn = false;
	picked_pi = -1;
	pick_drag_limit = INFINITY;

	renderer.camera_azimuth = CAMERA_AZIMUTH;
	renderer.camera_fov = CAMERA_FOV;
	renderer.p_camera = P_CAMERA;			// Camera position	
	renderer.u_camera = U_CAMERA;			// Camera up	
	renderer.r_camera = R_CAMERA;			// Camera right	
	renderer.l_camera = L_CAMERA;			// Camera lookAt

	renderer.cloth_bias = 1.0f;
	renderer.wireframe_bias = 0.999f;
	renderer.same_color = false;

	Load();	// load the configuration of the client 

	if (init_gl) InitGL();

	dynamo::create();
	dynamo::reset(); // set the parameters of Vivace to default values
	dynamo::load_config("../../demos/config/dynamo_config.xml");

	cout << demos[currDemo]->Name() << endl;
	cout << demos[currDemo]->Description() << endl;
	demos[currDemo]->Init(&renderer);

	cout << dynamo::get_version_number() << endl;

	InitRenderer();

}

void DynamoApp::Reset()
{
	dynamo::shutdown();
	dynamo::destroy();

	Init(false);

	glfwSetWindowTitle(window, "Dynamo SDK");
}

void DynamoApp::Close()
{
	/*ofstream file;
	file.open("sim_time.txt");

	for (int i = 0; i < sim_times.size(); i++)
	{
		file << sim_times[i] << endl;
	}

	file.close();*/

	//cputimer.evaluate();
	demos[currDemo]->Destroy();

	for (auto i = 0; i < demos.size(); i++)
	{
		delete demos[i];
	}

	gui.ShutDown();
	renderer.Shutdown();
	
	dynamo::shutdown();
	dynamo::destroy();

	glfwTerminate();
	Save();
}


void DynamoApp::Open(string& filename)
{
	Reset();
}


void DynamoApp::ExportMesh(string& filename)
{
	//vivace.ExportMeshToDisk(filename);
}

// handles key down event
void DynamoApp::KeyDown(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (ImGui::GetIO().WantCaptureKeyboard)
		return;

	if (action != GLFW_RELEASE)
		return;

	//	FILE_LOG(logDEBUG2) << "key " << key << " has been pressed";

	demos[currDemo]->KeyDown(key);

	switch (key)
	{
	case GLFW_KEY_W:
		renderer.wireframe_enabled = !renderer.wireframe_enabled;
		break;

	case GLFW_KEY_P:
		renderer.show_surface_particles = !renderer.show_surface_particles;
		break;

	case GLFW_KEY_S:
		renderer.Snapshot();
		break;

	case GLFW_KEY_LEFT:
		demos[currDemo]->Destroy();
		(currDemo == 0) ? currDemo = demos.size() - 1 : currDemo--;
		renderer.Reset();
		Reset();
		break;

	case GLFW_KEY_RIGHT:
		demos[currDemo]->Destroy();
		currDemo = (currDemo + 1) % demos.size();
		renderer.Reset();
		Reset();
		break;

	case GLFW_KEY_R:
		//Reset();
		dynamo::init();
		break;

	case GLFW_KEY_A:
	{
		float size = 4.0f;
		int resolution = 81;
		int object_id;
		dynamo::create_deformable_patch(size, size, resolution, resolution, object_id);

		dynamo::translate_object(object_id, 0.5f, -3.0f, 0);
		dynamo::rotate_object(object_id, M_PI, 0, 1, 0);
		dynamo::rotate_object(object_id, M_PI, 0, 0, 1);

		dynamo::init();
		//pVivace->scale_obj(id_object, 50, 50, 50);
		//for (unsigned int pi = 0; pi < res * res; pi++)
		//	pVivace->set_friction_obj(id_object, pi, 1.f);	// 0: no friction, 1: max friction

		break;
	}


	case GLFW_KEY_D:
	{
		// select a random animated object and remove it
		int num_objs = dynamo::get_num_deformable_objects();
		int count = 0;
		if (num_objs > 0)
		{
			int id = rand() % num_objs;
			while ((count < num_objs) && (!dynamo::is_enabled(id)))
			{
				id = (id + 1) % num_objs;
				count++;
			}

			if (count < num_objs)
				dynamo::delete_deformable_object(id);
		}
		break;
	}
	case GLFW_KEY_M:
	{
		//pVivace->ExportMeshToDisk("./mesh.obj\0");
		break;
	}
	case GLFW_KEY_C:
	{
		renderer.PrintCamera();
		break;
	}

	case GLFW_KEY_SPACE: // space
		bFreeze = !bFreeze;
		break;

	case GLFW_KEY_Q:
	case GLFW_KEY_ESCAPE:   // ESC
		glfwSetWindowShouldClose(window, GLFW_TRUE);
		break;

	case GLFW_KEY_O:
	{
		dynamo::update();
		break;
	}
	}

	
}


void DynamoApp::Reshape()
{
	glfwGetFramebufferSize(window, &win_width, &win_height);

	renderer.Reshape(win_width, win_height);
}

// handles when a mouse button is pressed / released
void DynamoApp::MouseClick(GLFWwindow* window, int button, int action, int mods)
{
	double x, y;
	glfwGetCursorPos(window, &x, &y);
	mouse2D = Vector3x(Scalar(x), Scalar(win_height - y), 0);

	if (ImGui::GetIO().WantCaptureMouse)
		return;

	//	FILE_LOG(logDEBUG2) << "mouse key has changed state, pos " << x << " , " << y;


	if (action == GLFW_RELEASE)
	{
		// stop dragging
		if (button == GLFW_MOUSE_BUTTON_RIGHT)
			isDragging = false;

		picked_pi = -1;

		// Turn-off rotations and zoom.
		Movement = MovementType::NONE;
		return;
	}

	switch (button)
	{
	case (GLFW_MOUSE_BUTTON_LEFT):

		// Turn on user interactive rotations.
		// As the user moves the mouse, the scene will rotate.
		Movement = ROTATE;

		isDragging = false;	// turn off picking anyway

							// Map the mouse position to a logical sphere location.
							// Keep it in the class variable lastPoint.
		lastPoint = trackBallMapping((int)x, (int)y);

		//// Make sure we are modifying the MODELVIEW matrix.
		//glMatrixMode( GL_MODELVIEW );
		break;

	case (GLFW_MOUSE_BUTTON_MIDDLE):

		isDragging = false;

		// Turn on user interactive zooming.
		// As the user moves the mouse, the scene will zoom in or out
		//   depending on the x-direction of travel.
		//		if (glutGetModifiers() == GLUT_ACTIVE_CTRL)
		if (mods && GLFW_MOD_CONTROL)
			Movement = PAN;
		else
			Movement = ZOOM;

		// Set the last point, so future mouse movements can determine
		//   the distance moved.
		lastPoint.x() = Scalar(x);
		lastPoint.y() = Scalar(y);
		break;

	case (GLFW_MOUSE_BUTTON_RIGHT):
		// enable picking of a particle
		Movement = MovementType::NONE;
		// TODO: move this in Vivace
		if (mods & GLFW_MOD_CONTROL) {
			selectingFixed = true;
		}

		pickOn = true;
		break;
	}
}


// handle any necessary mouse movements through the trackball
void DynamoApp::MouseMotion(GLFWwindow* window, double x, double y)
{
	Vector3x direction;
	double pixel_diff;
	double zoom_factor;
	Vector3x curPoint;

	if (ImGui::GetIO().WantCaptureMouse)
		return;


	switch (Movement)
	{
	case ROTATE:  // Left-mouse button is being held down
	{
		curPoint = trackBallMapping((int)x, (int)y);  // Map the mouse position to a logical sphere location.
		direction = curPoint - lastPoint;
		if (direction.x() > 75) direction.x() = 75;
		if (direction.x() < -75) direction.x() = -75;
		double velocity = direction.norm();

		if (velocity > 0.0001)
		{
			vcg::Quaternionf q_up, q_right;
			q_right.FromAxis(float(vcg::math::ToRad(direction.y() * m_ROTSCALE)), FromVector3x(renderer.r_camera));

			vcg::Point3f p_cam;
			p_cam = q_right.Rotate(FromVector3x(renderer.p_camera));
			renderer.p_camera = Vector3x(p_cam.X(), p_cam.Y(), p_cam.Z());

			renderer.camera_azimuth += float(direction.x() * m_ROTSCALE);
			renderer.UpdateCamera();
		}
		break;
	}
	case ZOOM:
	{
		pixel_diff = y - lastPoint.y();
		zoom_factor = 1.0 + pixel_diff * m_ZOOMSCALE;

		float dist = (float)(renderer.p_camera - renderer.l_camera).norm();
		Vector3x dir(renderer.p_camera - renderer.l_camera);
		dir.normalize();
		renderer.p_camera = renderer.l_camera + dir * dist * zoom_factor;

		// Set the current point, so the lastPoint will be saved properly below.
		curPoint.x() = Scalar(x);  curPoint.y() = Scalar(y);  curPoint.z() = 0;
	}
	break;

	case PAN:
		curPoint.x() = static_cast<Scalar>(x);  curPoint.y() = static_cast<Scalar>(y);  curPoint.z() = 0;
		direction = curPoint - lastPoint;

		renderer.l_camera += renderer.r_camera * direction.x() * .1f;
		renderer.l_camera += renderer.u_camera * direction.y() * .1f;
		renderer.p_camera += renderer.r_camera * direction.x() * .1f;
		renderer.p_camera += renderer.u_camera * direction.y() * .1f;
		renderer.UpdateCamera();
		break;
	}

	// Save the location of the current point for the next movement. 
	lastPoint = curPoint;	// in spherical coordinates
	mouse2D = Vector3x(Scalar(x), Scalar(win_height - y), 0);	// in window coordinates
}

// draw the scene
void DynamoApp::Draw()
{
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	renderer.Draw(width, height);
}


void DynamoApp::InitRenderer()
{
	//glfwSwapInterval(2);	// swap buffer every other frame
	renderer.Init(win_width, win_height);
}

void DynamoApp::Update()
{
	//cputimer.startEvent("DynamoApp::Update()");
	if (!bFreeze)
	{
		if ((picked_pi != -1) && isDragging) 
		{			
			dynamo::set_invmass(picked_pi, 0);
			dynamo::move_particle_limited(picked_pi, mouse3D[0], mouse3D[1], mouse3D[2], 
										  picked_pos.x(), picked_pos.y(), picked_pos.z());
		}
		
		dynamo::update();
		demos[currDemo]->Update(&renderer);		

		cur_frame++;
	}

	// release the picked particle
	if ((picked_pi != -1) && isDragging) {
		dynamo::set_invmass(picked_pi, 1);	// todo(marco): this is wrong, the mass of the particle may be not 1..
	}	
		

	/*if (cur_frame >= end_frame) {
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}*/
	//cputimer.endEvent();
}


void KeyDownCallBack(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	DynamoApp * pApp = (DynamoApp*)glfwGetWindowUserPointer(window);
	pApp->KeyDown(window, key, scancode, action, mods);
}

void MouseClickCallBack(GLFWwindow* window, int button, int action, int mods)
{
	DynamoApp * pApp = (DynamoApp*)glfwGetWindowUserPointer(window);
	pApp->MouseClick(window, button, action, mods);
}

void MouseMotionCallBack(GLFWwindow* window, double x, double y)
{
	DynamoApp * pApp = (DynamoApp*)glfwGetWindowUserPointer(window);
	pApp->MouseMotion(window, x, y);
}

void ReshapeCallBack(GLFWwindow* window, int, int)
{
	DynamoApp * pApp = (DynamoApp*)glfwGetWindowUserPointer(window);
	pApp->Reshape();
}

GLFWwindow * g_window = NULL;	// this is used to bind c-style functions in ImGui to member functions in DynamoApp -- ugly but effective

void ResetCallBack()
{
	DynamoApp * pApp = (DynamoApp*)glfwGetWindowUserPointer(g_window);
	pApp->Reset();
}

void OpenCallBack(std::string & s)
{
	DynamoApp * pApp = (DynamoApp*)glfwGetWindowUserPointer(g_window);
	pApp->Open(s);
}

void ExportMeshCallBack(std::string & s)
{
	DynamoApp * pApp = (DynamoApp*)glfwGetWindowUserPointer(g_window);
	pApp->ExportMesh(s);
}

void DynamoApp::InitGL()
{
	isDragging = false;
	picked_pi = -1;
	Movement = MovementType::NONE;

	// Setup window
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		//FILE_LOG(logINFO) << "GL initialization failed.";
		Close();
	}

	glfwWindowHint(GLFW_SAMPLES, 4);	// enable multisample
	glEnable(GL_MULTISAMPLE);

	static int iters = 0;

	if (iters == 0)
		window = glfwCreateWindow(win_width, win_height, "Dynamo SDK", NULL, NULL);
	iters++;

	if (!window)
	{
		//FILE_LOG(logINFO) << "Window or OpenGL context creation failed, this may depend on properly installed drivers.";
		Close();
	}
	glfwMakeContextCurrent(window);
	glfwSetWindowUserPointer(window, this);
	g_window = window;

	GLenum res = glewInit();
	if (res != 0)
	{
		//FILE_LOG(logINFO) << "GLEW failed to initialize, maybe old graphics card or drivers?\n";
		Close();
	}

	gui.Init(window, demos);

	gui.SetResetFunction(ResetCallBack);
	gui.SetOpenFunction(OpenCallBack);
	gui.SetExportFunction(ExportMeshCallBack);

	glfwSetKeyCallback(window, KeyDownCallBack);
	glfwSetMouseButtonCallback(window, MouseClickCallBack);
	glfwSetCursorPosCallback(window, MouseMotionCallBack);
	glfwSetFramebufferSizeCallback(window, ReshapeCallBack);
}

void DynamoApp::Run()
{
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
		glClearColor(gui.clear_color.x, gui.clear_color.y, gui.clear_color.z, gui.clear_color.w);
		double start_frame = GetRealTimeInMS();
		// Update the sim/draw_time less frequent so that
		// the fps is readable
		// TODO: Frequency should be dependent on fps
		
		Update();
		if (cur_frame % 50 == 0) {
			sim_time = GetRealTimeInMS() - start_frame;
			//sim_times.push_back(sim_time);
		}
		Draw();
		if (cur_frame % 50 == 0) {
			draw_time = GetRealTimeInMS() - start_frame - sim_time;
		}

		int selectedDemo = (int) currDemo;

		gui.Update(&renderer, (int) currDemo, selectedDemo, bFreeze, (float)sim_time, (float)draw_time);

		if (selectedDemo != currDemo)
		{
			demos[currDemo]->Destroy();
			currDemo = selectedDemo;
			renderer.Reset();
			Reset();
		}

		gui.Render();
#if 1
		// todo: picking and trackball belongs to somewhere else... refactor
		// if picking enabled (right mouse button pressed), handle the picking
		if (pickOn)
		{

			double	p[3], q[3];
			renderer.Get_Selection_Ray((int)mouse2D.x(), (int)mouse2D.y(), p, q);

			Vector3x _p((Scalar)p[0], (Scalar)p[1], (Scalar)p[2]);	// dumb data conversion; TODO: convert to eigen or glm
			Vector3x _q((Scalar)q[0], (Scalar)q[1], (Scalar)q[2]);

			// [CUDA Interop]
			// If we run GPU computing, with interop, we need to copy down all
			// vertex positions to the CPU to do the picking
			/*if (pVivace->get_computation_type() == ComputationType::GPU_CUDA) {
				pVivace->copy_to_cpu();
			}*/

			renderer.Select(p, q, picked_pi, ds);
			if (picked_pi != -1 && dynamo::get_invmass(picked_pi) != 0)
			{
				cout << "picked particle " << picked_pi << endl;
				if (selectingFixed)
				{
					selectingFixed = false;
				}
				else
				{
					dynamo::get_physics_position(picked_pi, picked_pos[0], picked_pos[1], picked_pos[2]);
					isDragging = true;
				}
			}

			pickOn = false;
		}

		// while dragging a particle, draw it together with the current mouse cursor in 3D coordinates
		if (isDragging)
		{
			// find mouse position in 3D
			GLdouble p[3];
			GLdouble mm[16], prm[16];
			int viewport[4];
			glGetIntegerv(GL_VIEWPORT, viewport);
			glGetDoublev(GL_MODELVIEW_MATRIX, mm);
			glGetDoublev(GL_PROJECTION_MATRIX, prm);

			gluUnProject(mouse2D.x(), mouse2D.y(), ds, mm, prm, viewport, &p[0], &p[1], &p[2]);

			mouse3D = Vector3x((Scalar)p[0], (Scalar)p[1], (Scalar)p[2]);	// mouse position in 3D

			// draw picked particle and mouse position
			glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glEnable(GL_LIGHTING);

			Scalar x, y, z;
			dynamo::get_physics_position(picked_pi, x, y, z);
			Vector3x picked_pos(x, y, z);

			glPushMatrix();
			glColor3f(0.f, 1.f, 0.f);
			Renderer::glTranslate(picked_pos);
			gluSphere(gluNewQuadric(), .05, 10, 10);
			glPopMatrix();

			glPushMatrix();
			Renderer::glTranslate(mouse3D);
			glColor3f(1.f, 0.f, 0.f);
			gluSphere(gluNewQuadric(), .05, 10, 10);
			glPopMatrix();

			glDisable(GL_LIGHTING);
			glColor3f(0.f, 0.f, 1.f);
			glBegin(GL_LINES);
			Renderer::glVertex3(picked_pos);
			Renderer::glVertex3(mouse3D);
			glEnd();
			glPopAttrib();

		}
		renderer.Finalize();
#endif

		//	if ((cur_frame % 20) == 0)
		glfwSwapBuffers(window);
	}
	Close();

}


//
// Utility routine to calculate the 3D position of a 
// projected unit vector onto the xy-plane. Given any
// point on the xy-plane, we can think of it as the projection
// from a sphere down onto the plane. The inverse is what we
// are after.
//
Vector3x DynamoApp::trackBallMapping(int x, int y)
{
	return Vector3x((Scalar)x, (Scalar)y, 0);

	Vector3x v;
	Scalar d;

	v.x() = (Scalar)(2.0 * x - win_width) / win_width;
	v.y() = (Scalar)(win_height - 2.0 * y) / win_height;
	v.z() = 0.0;
	d = v.norm();
	d = (d < 1.f) ? d : 1.f;  // If d is > 1, then clamp it at one.
	v.z() = (Scalar)sqrtf(float(1.001 - d * d));  // project the line segment up to the surface of the sphere.

	v.normalize();  // We forced d to be less than one, not v, so need to normalize somewhere.
	return v;

}


