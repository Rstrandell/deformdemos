#pragma once

#include <GL/glew.h>
#include "types.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

using namespace std;

#define ZERO_MEM(a) memset(a, 0, sizeof(a))
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

class AnimationImporter
{
#define NUM_BONES_PER_VERTEX 8

	struct BoneInfo
	{
		Eigen::Matrix4f BoneOffset;
		Eigen::Matrix4f FinalTransformation;
	};

	struct VertexBoneData
	{
		unsigned int IDs[NUM_BONES_PER_VERTEX];
		float Weights[NUM_BONES_PER_VERTEX];

		VertexBoneData()
		{
			Reset();
		};

		void Reset()
		{
			ZERO_MEM(IDs);
			ZERO_MEM(Weights);
		}

		void AddBoneData(unsigned int BoneID, float Weight);
	};

	void CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	
	unsigned int FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
	
	const aiNodeAnim* FindNodeAnim(const aiAnimation* pAnimation, const string NodeName);
	
	void ReadNodeHierarchy(float AnimationTime, const aiNode* pNode, const Eigen::Matrix4f& ParentTransform);
	
	void InitMesh(unsigned int MeshIndex,
		const aiMesh* paiMesh,
		vector<Vector3f>& Positions,
		vector<Vector3f>& Normals,
		vector<VertexBoneData>& Bones,
		vector<int>& Indices);
	
	void BoneTransform(float TimeInSeconds);
	void LoadBones(unsigned int MeshIndex, const aiMesh* paiMesh, vector<VertexBoneData>& Bones);
	Eigen::Matrix4f AiMatrix4x4ToEigenMatrix4f(aiMatrix4x4 m);

#define INVALID_MATERIAL 0xFFFFFFFF

	struct MeshEntry {
		MeshEntry()
		{
			NumIndices = 0;
			BaseVertex = 0;
			BaseIndex = 0;
			MaterialIndex = INVALID_MATERIAL;
		}

		unsigned int NumIndices;
		unsigned int BaseVertex;
		unsigned int BaseIndex;
		unsigned int MaterialIndex;
	};

	vector<MeshEntry> m_Entries;

	map<string, unsigned int> m_BoneMapping; // maps a bone name to its index
	unsigned int m_NumBones;
	vector<BoneInfo> m_BoneInfo;
	Eigen::Matrix4f m_GlobalInverseTransform;

	const aiScene* m_pScene;
	Assimp::Importer m_Importer;

	vector<Vector3x> Positions;
	vector<Vector3x> Normals;
	vector<VertexBoneData> Bones;
	vector<int> Indices;

public:
	bool LoadAnimation(string file_path, vector<float> & vertices, vector<float> & normals, vector<int> & triangles,
					   float t_x, float t_y, float t_z, float r_angle, float r_x, float r_y, float r_z, float s_x, float s_y, float s_z);
	
	bool InitFromScene(const aiScene *scene);
	void GetAnimationAtTime(float t, vector<float> & vertices, vector<float> & normals);
	void InterpolateToFirstFrame(float t, vector<float> & vertices, vector<float> & normals);
};