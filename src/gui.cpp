#include "gui.h"
#include "renderer.h"

#include "dynamo_api.h"

void GUI::Init(GLFWwindow* window, vector<Demo*> d)
{
	IMGUI_CHECKVERSION();

	ImGui::CreateContext();

	// Setup ImGui binding
	this->window = window;

	ImGui::StyleColorsClassic();
	
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL2_Init();
	
	//ImGuiIO& io = ImGui::GetIO();
	//io.Fonts->AddFontFromFileTTF(DATA_FOLDER "RobotoMono-Regular.ttf", 20);

	show_test_window = true;
	show_another_window = false;
	clear_color = ImColor(180, 180, 190);

	demos = d;
	demo_names.clear();

	for (auto i = 0; i < demos.size(); i++)
	{
		demo_names.push_back(demos[i]->Name());
	}
}


void GUI::SetResetFunction(void(*reset)())
{
	this->reset = reset;
}

void GUI::SetOpenFunction(void(*open)(string&))
{
	this->open = open;
}

void GUI::SetExportFunction(void(*export_mesh)(string&))
{
	this->export_mesh = export_mesh;
}

void GUI::Update(Renderer * renderer, int curr_demo, int & selected_demo, bool & bFreeze, float sim_time, float draw_time)
{
	ImGui_ImplOpenGL2_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	ImGui::GetIO().FontGlobalScale = 1.f;

	//// main menu
	//if (ImGui::BeginMainMenuBar())
	//{
	//	if (ImGui::BeginMenu("File"))
	//	{
	//		MenuFile();
	//		ImGui::EndMenu();
	//	}
	//	//if (ImGui::BeginMenu("Edit"))
	//	//{
	//	//	if (ImGui::MenuItem("Undo", "CTRL+Z")) {}
	//	//	if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
	//	//	ImGui::Separator();
	//	//	if (ImGui::MenuItem("Cut", "CTRL+X")) {}
	//	//	if (ImGui::MenuItem("Copy", "CTRL+C")) {}
	//	//	if (ImGui::MenuItem("Paste", "CTRL+V")) {}
	//	//	ImGui::EndMenu();
	//	//}
	//	ImGui::EndMainMenuBar();
	//}
		int width, height;
		glfwGetWindowSize(window, &width, &height);

	{
		ImGui::SetNextWindowPos(ImVec2(10.f, 10.f));
		
		bool open;
		ImGui::Begin("Controls", &open, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_AlwaysAutoResize);

		ImGui::Text("Controls:");
		ImGui::Separator();
		ImGui::Text("[ Left click + drag ] : Rotate");
		ImGui::Text("[ Middle click + drag ] : Zoom");
		ImGui::Text("[ Right click + drag ] : Grab and pull");

		ImGui::Separator();
		ImGui::Text("[ Left/Right ] : change demo");
		ImGui::Text("[ Space ] : freeze/unfreeze");
		ImGui::Text("[ R ] : reset");
		ImGui::Text("[ W ] : toggle wireframe on/off");

		ImGui::Separator();
		ImGui::Text("Current Demo:");
		ImGui::Text(demos[curr_demo]->Name());
		ImGui::Text(demos[curr_demo]->Description());

		ImGui::Separator();

		ImGui::PushItemWidth(300);
		ImGui::Combo("Current scene", &selected_demo, demo_names.data(), (int)demo_names.size());

		ImGui::End();
	}

	{
		ImGui::SetNextWindowPos(ImVec2(width - 360.f, height - 140.f));
		ImGui::Begin("Parameters", &show_another_window, ImGuiWindowFlags_AlwaysAutoResize);
		
		{
			Scalar grav[3];
			dynamo::get_gravity(grav[0], grav[1], grav[2]);
			
			float fgrav[3] = {(float)grav[0], (float)grav[1], (float)grav[2]};

			ImGui::SliderFloat3("gravity", fgrav, -10.0f, 10.0f);
			dynamo::set_gravity((Scalar)fgrav[0], (Scalar)fgrav[1], (Scalar)fgrav[2]);
		}
		{
			Scalar wind[3];
			dynamo::get_wind(wind[0], wind[1], wind[2]);

			float fwind[3] = { (float)wind[0], (float)wind[1], (float)wind[2] };

			ImGui::SliderFloat3("wind", fwind, -5.0f, 5.0f);
			dynamo::set_wind((Scalar)fwind[0], (Scalar)fwind[1], (Scalar)fwind[2]);
		}

		float v = (float) dynamo::get_air_friction();
		ImGui::SliderFloat("friction kinematic", &v, 0.00f, 0.05f, "%.6f");
		dynamo::set_air_friction(v);

		ImGui::End();
	}

	/*{
		ImGui::SetNextWindowPos(ImVec2(width - 360.f, height - 270.f));
		ImGui::Begin("Curve", &show_another_window, ImGuiWindowFlags_AlwaysAutoResize);

		{
			Vector3x a;
			Vector3x b;
			Vector3x c;
			Vector3x d;
			pVivace->get_curve(0, a, b, c, d);

			float f_a[3] = { (float)a[0], (float)a[1], (float)a[2] };
			float f_b[3] = { (float)b[0], (float)b[1], (float)b[2] };
			float f_c[3] = { (float)c[0], (float)c[1], (float)c[2] };
			float f_d[3] = { (float)d[0], (float)d[1], (float)d[2] };

			ImGui::SliderFloat3("a", f_a, -5.0f, 5.0f);
			ImGui::SliderFloat3("b", f_b, -5.0f, 5.0f);
			ImGui::SliderFloat3("c", f_c, -5.0f, 5.0f);
			ImGui::SliderFloat3("d", f_d, -5.0f, 5.0f);

			Vector3x new_a = Vector3x(f_a[0], f_a[1], f_a[2]);
			Vector3x new_b = Vector3x(f_b[0], f_b[1], f_b[2]);
			Vector3x new_c = Vector3x(f_c[0], f_c[1], f_c[2]);
			Vector3x new_d = Vector3x(f_d[0], f_d[1], f_d[2]);

			pVivace->set_curve(0, new_a, new_b, new_c, new_d);
		}

		ImGui::End();
	}*/

	{
		int width, height;
		glfwGetWindowSize(window, &width, &height);
		ImGui::SetNextWindowPos(ImVec2(10.f, height - 140.f));
		bool open;
		ImGui::Begin("Simple Profiler", &open, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_AlwaysAutoResize);

		ComputationType ct = (ComputationType)dynamo::get_computation_type();

		if (ct == CPU)
			ImGui::Text("Computation type: Single-core CPU");
		if (ct == CPU_MULTI)
			ImGui::Text("Computation type: Multi-core CPU");
		if (ct == GPU_CUDA)
			ImGui::Text("Computation type: GPU CUDA");
		ImGui::Separator();
		//ImGui::Text("gui %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		//ImGui::Text("sim %.3f ms/frame (%.1f FPS)", sim_time, 1000.f / sim_time);
		//ImGui::Text("drw %.3f ms/frame (%.1f FPS)", draw_time, 1000.f / draw_time);

		ImGui::Text("%.3f ms/frame (%.1f FPS)", draw_time + sim_time, 1000.f / (draw_time + sim_time));
		ImGui::Separator();
		//if (ps->_tetras.size() > 0)
		//	ImGui::Text("vertices: %d   faces: %d   tetras: %d", (int)ps->pos.size(), (int)ps->faces.size() / 3, (int)ps->_tetras.size() / 4);
		//else
		ImGui::Text("vertices: %d   faces: %d", (int)dynamo::get_num_physics_vertices(), (int)dynamo::get_num_physics_indices() / 3);
		ImGui::Separator();
		ImGui::Text("Solver iterations:  %d", (int)dynamo::get_solver_iterations());
		ImGui::Text("Time steps: %d", (int)dynamo::get_timesteps_per_frame());
		//if ((ps->solve_types[DISTANCE]) && (ps->stretch_stiffness > 0))
		//	ImGui::Text("constraints[stretch]:   %d", ps->rest_val[DISTANCE].size());

		//if ((ps->solve_types[BENDING]) && (ps->bending_stiffness > 0))
		//	ImGui::Text("constraints[bend]:      %d", ps->rest_val[BENDING].size());

		//if ((ps->solve_types[TETRA_VOLUME]) && (ps->tetra_stiffness > 0))
		//	ImGui::Text("constraints[volume]:    %d", ps->rest_val[TETRA_VOLUME].size());

		ImGui::End();
	}


	////// 1. Show a simple window
	////// Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
	//{
	//	static float f = 0.0f;
	//	ImGui::Text("Hello, world!");
	//	ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
	//	ImGui::ColorEdit3("clear color", (float*)&clear_color);
	//	if (ImGui::Button("Test Window")) show_test_window ^= 1;
	//	if (ImGui::Button("Another Window")) show_another_window ^= 1;
	//	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	//}

	//// 2. Show another simple window, this time using an explicit Begin/End pair
	//if (show_another_window)
	//{
	//	ImGui::SetNextWindowSize(ImVec2(200, 100), ImGuiSetCond_FirstUseEver);
	//	ImGui::Begin("Another Window", &show_another_window);
	//	ImGui::Text("Hello");
	//	ImGui::End();
	//}

	//// 3. Show the ImGui test window. Most of the sample code is in ImGui::ShowTestWindow()
	//if (show_test_window)
	//{
	//	ImGui::SetNextWindowPos(ImVec2(650, 20), ImGuiSetCond_FirstUseEver);
	//	ImGui::ShowTestWindow(&show_test_window);
	//}
}

void GUI::Render()
{
	//// Rendering

	//glMatrixMode(GL_PROJECTION);
	//glLoadIdentity();
	//glMatrixMode(GL_MODELVIEW);
	//glLoadIdentity();

	//int display_w, display_h;
	//glfwGetFramebufferSize(window, &display_w, &display_h);
	//glViewport(0, 0, display_w, display_h);
	glDisable(GL_LIGHTING);
	ImGui::Render();
	ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());
}

void GUI::ShutDown()
{
	ImGui_ImplOpenGL2_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}