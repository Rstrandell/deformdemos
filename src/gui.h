#include <imgui.h>
#include <stdio.h>
#include <string>

#include "demo.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl2.h"

using namespace std;

class Renderer;
class Demo;

class GUI
{
	typedef void(*callback_function)(void);
	typedef void(*callback_function_1s)(string&);	// usgly typedef for function passing a string by reference

	// test variables
	// TODO: remove
	bool show_test_window;
	bool show_another_window;

	GLFWwindow* window;
	vector<Demo*> demos;
	vector<const char *> demo_names;

	callback_function reset;
	callback_function_1s open;
	callback_function_1s export_mesh;

public:
	ImVec4 clear_color;

	void Init(GLFWwindow* window, vector<Demo*> demos);
	void Update(Renderer * renderer, int curr_demo, int & selected_demo, bool & bFreeze, float sim_time, float draw_time);
	void Render();
	void ShutDown();

	// call this just after init
	void SetResetFunction(void(*reset)());
	void SetOpenFunction(void(*open)(string&));
	void SetExportFunction(void(*export_mesh)(string&));
};