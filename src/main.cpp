#include "app.h"

#include "types.h"
#include <stdio.h>

using namespace std;

int main(int argc, char** argv)
{
	DynamoApp app;
	app.Init(true);
	app.Run();

	return 0;
}
