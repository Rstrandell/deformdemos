#ifndef __DEMO__
#define __DEMO__

#include <string>
#include <vector>

#include "timer.h"

#include "types.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

#ifndef DATA_FOLDER
#define DATA_FOLDER "../../demos/data/"
#endif

using namespace std;

class Renderer;

class Demo
{
public:

	virtual void Init(Renderer* pRenderer) = 0;
	virtual void Update(Renderer* pRenderer) = 0;
	virtual void KeyDown(int key){}
	virtual void Destroy(){}

	virtual const char * Name() = 0;
	virtual const char * Description() = 0;

	// helper function to load an obj file; it uses tinyobjloader
	static bool LoadObj(const char* filename, vector<Scalar> & vert, vector<int> & tri);
	static bool LoadObj(const char* filename, vector<Scalar> & vert, vector<Scalar> & norm, vector<int> & tri);

	static void LoadSerializedHelper(const char* filename);
protected:
};

#endif