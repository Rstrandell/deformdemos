#ifndef __DYNAMO_APP__
#define __DYNAMO_APP__

#include <string>
#include <vector>

#include "types.h"

#include "renderer.h"
#include "gui.h"

#include "demo.h"

//#include "cpu_timer.h"

using namespace std;

class DynamoApp
{
	enum MovementType { ROTATE, ZOOM, PAN, NONE };  // Keep track of the current mode of interaction (which mouse button is pressed)

	GLFWwindow * window;
	int win_width;
	int win_height;

	//CpuTimer cputimer;

	float fps;

	bool bFreeze = true; // if true, freeze the simulation - start editing mode

	int cur_frame;
	int end_frame;

	// used to pick a particle and to drag it
	int picked_pi;	// the index of the selected particle
	double ds;	// distance of the picked primitive from the mouse cursor
	bool pickOn;
	bool isDragging;
	Vector3x mouse2D;
	Vector3x mouse3D;
	
	Vector3x picked_pos;
	float pick_drag_limit;

	Vector3x trackBallMapping(int x, int y);	// Utility routine to convert mouse locations to a virtual hemisphere
	Vector3x lastPoint;							// Keep track of the last mouse location
	MovementType Movement;						// Left-mouse => ROTATE, Right-mouse => ZOOM

	bool selectingFixed;	// if true, the selected particle will become fixed

	Renderer renderer;
	GUI gui;

	vector<Demo*> demos;	// all the demos preloaded in the application
	size_t currDemo;		// index of the current demo

	vector<float> sim_times;
public:

	DynamoApp();

	void Init(bool init_gl);
	void Reset();
	void Close();

	void Open(string& filename);	// todo(marco): DEPRECATED .. remove, use load and save instead
	void ExportMesh(string& filename);

	void Reshape();
	void KeyDown(GLFWwindow* window, int key, int scancode, int action, int mods);
	void MouseClick(GLFWwindow* window, int button, int action, int mods);
	void MouseMotion(GLFWwindow* window, double x, double y);

	void Draw();
	void InitRenderer();
	void InitGL();
	void Update();

	void Run();

	// todo(marco): make this private
	template <class Archive>
	void serialize(Archive & ar);

	void Load();
	void Save();
};

#endif