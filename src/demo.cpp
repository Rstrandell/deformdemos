#include "demo.h"

#include "types.h"
#include "dynamo_api.h"

#ifndef TINYOBJLOADER_IMPLEMENTATION
#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h"
#endif

void PrintInfoObj(const tinyobj::attrib_t& attrib, const std::vector<tinyobj::shape_t>& shapes, const std::vector<tinyobj::material_t>& materials)
{
	cout << "# of vertices  : " << (attrib.vertices.size() / 3) << endl;
	cout << "# of normals   : " << (attrib.normals.size() / 3) << endl;
	cout << "# of texcoords : " << (attrib.texcoords.size() / 2) << endl;
	
	cout << "# of shapes    : " << shapes.size() << endl;
	cout << "# of materials : " << materials.size() << endl;

	if (0)
		for (size_t v = 0; v < attrib.vertices.size() / 3; v++) {
			printf("  v[%ld] = (%f, %f, %f)\n", static_cast<long>(v),
				static_cast<const double>(attrib.vertices[3 * v + 0]),
				static_cast<const double>(attrib.vertices[3 * v + 1]),
				static_cast<const double>(attrib.vertices[3 * v + 2]));
		}

	if (0)
		for (size_t v = 0; v < attrib.normals.size() / 3; v++) {
			printf("  n[%ld] = (%f, %f, %f)\n", static_cast<long>(v),
				static_cast<const double>(attrib.normals[3 * v + 0]),
				static_cast<const double>(attrib.normals[3 * v + 1]),
				static_cast<const double>(attrib.normals[3 * v + 2]));
		}

	if (0)
		for (size_t v = 0; v < attrib.texcoords.size() / 2; v++) {
			printf("  uv[%ld] = (%f, %f)\n", static_cast<long>(v),
				static_cast<const double>(attrib.texcoords[2 * v + 0]),
				static_cast<const double>(attrib.texcoords[2 * v + 1]));
		}

	// For each shape
	for (size_t i = 0; i < shapes.size(); i++) {
		printf("shape[%ld].name = %s\n", static_cast<long>(i),
			shapes[i].name.c_str());
		printf("Size of shape[%ld].indices: %lu\n", static_cast<long>(i),
			static_cast<unsigned long>(shapes[i].mesh.indices.size()));

		size_t index_offset = 0;

		assert(shapes[i].mesh.num_face_vertices.size() ==
			shapes[i].mesh.material_ids.size());

		printf("shape[%ld].num_faces: %lu\n", static_cast<long>(i),
			static_cast<unsigned long>(shapes[i].mesh.num_face_vertices.size()));

		// For each face
		if (0)
			for (size_t f = 0; f < shapes[i].mesh.num_face_vertices.size(); f++) {
				size_t fnum = shapes[i].mesh.num_face_vertices[f];

				printf("  face[%ld].fnum = %ld\n", static_cast<long>(f),
					static_cast<unsigned long>(fnum));

				// For each vertex in the face
				for (size_t v = 0; v < fnum; v++) {
					tinyobj::index_t idx = shapes[i].mesh.indices[index_offset + v];
					printf("    face[%ld].v[%ld].idx = %d/%d/%d\n", static_cast<long>(f),
						static_cast<long>(v), idx.vertex_index, idx.normal_index,
						idx.texcoord_index);
				}

				printf("  face[%ld].material_id = %d\n", static_cast<long>(f),
					shapes[i].mesh.material_ids[f]);

				index_offset += fnum;
			}

		printf("shape[%ld].num_tags: %lu\n", static_cast<long>(i),
			static_cast<unsigned long>(shapes[i].mesh.tags.size()));

		if (0)
			for (size_t t = 0; t < shapes[i].mesh.tags.size(); t++) {
				printf("  tag[%ld] = %s ", static_cast<long>(t),
					shapes[i].mesh.tags[t].name.c_str());
				printf(" ints: [");
				for (size_t j = 0; j < shapes[i].mesh.tags[t].intValues.size(); ++j) {
					printf("%ld", static_cast<long>(shapes[i].mesh.tags[t].intValues[j]));
					if (j < (shapes[i].mesh.tags[t].intValues.size() - 1)) {
						printf(", ");
					}
				}
				printf("]");

				printf(" floats: [");
				for (size_t j = 0; j < shapes[i].mesh.tags[t].floatValues.size(); ++j) {
					printf("%f", static_cast<const double>(
						shapes[i].mesh.tags[t].floatValues[j]));
					if (j < (shapes[i].mesh.tags[t].floatValues.size() - 1)) {
						printf(", ");
					}
				}
				printf("]");

				printf(" strings: [");
				for (size_t j = 0; j < shapes[i].mesh.tags[t].stringValues.size(); ++j) {
					printf("%s", shapes[i].mesh.tags[t].stringValues[j].c_str());
					if (j < (shapes[i].mesh.tags[t].stringValues.size() - 1)) {
						printf(", ");
					}
				}
				printf("]");
				printf("\n");
			}
	}

	for (size_t i = 0; i < materials.size(); i++) {
		printf("material[%ld].name = %s\n", static_cast<long>(i),
			materials[i].name.c_str());
		printf("  material.Ka = (%f, %f ,%f)\n",
			static_cast<const double>(materials[i].ambient[0]),
			static_cast<const double>(materials[i].ambient[1]),
			static_cast<const double>(materials[i].ambient[2]));
		printf("  material.Kd = (%f, %f ,%f)\n",
			static_cast<const double>(materials[i].diffuse[0]),
			static_cast<const double>(materials[i].diffuse[1]),
			static_cast<const double>(materials[i].diffuse[2]));
		printf("  material.Ks = (%f, %f ,%f)\n",
			static_cast<const double>(materials[i].specular[0]),
			static_cast<const double>(materials[i].specular[1]),
			static_cast<const double>(materials[i].specular[2]));
		printf("  material.Tr = (%f, %f ,%f)\n",
			static_cast<const double>(materials[i].transmittance[0]),
			static_cast<const double>(materials[i].transmittance[1]),
			static_cast<const double>(materials[i].transmittance[2]));
		printf("  material.Ke = (%f, %f ,%f)\n",
			static_cast<const double>(materials[i].emission[0]),
			static_cast<const double>(materials[i].emission[1]),
			static_cast<const double>(materials[i].emission[2]));
		printf("  material.Ns = %f\n",
			static_cast<const double>(materials[i].shininess));
		printf("  material.Ni = %f\n", static_cast<const double>(materials[i].ior));
		printf("  material.dissolve = %f\n",
			static_cast<const double>(materials[i].dissolve));
		printf("  material.illum = %d\n", materials[i].illum);
		printf("  material.map_Ka = %s\n", materials[i].ambient_texname.c_str());
		printf("  material.map_Kd = %s\n", materials[i].diffuse_texname.c_str());
		printf("  material.map_Ks = %s\n", materials[i].specular_texname.c_str());
		printf("  material.map_Ns = %s\n",
			materials[i].specular_highlight_texname.c_str());
		printf("  material.map_bump = %s\n", materials[i].bump_texname.c_str());
		printf("  material.map_d = %s\n", materials[i].alpha_texname.c_str());
		printf("  material.disp = %s\n", materials[i].displacement_texname.c_str());
		printf("  <<PBR>>\n");
		printf("  material.Pr     = %f\n", materials[i].roughness);
		printf("  material.Pm     = %f\n", materials[i].metallic);
		printf("  material.Ps     = %f\n", materials[i].sheen);
		printf("  material.Pc     = %f\n", materials[i].clearcoat_thickness);
		printf("  material.Pcr    = %f\n", materials[i].clearcoat_thickness);
		printf("  material.aniso  = %f\n", materials[i].anisotropy);
		printf("  material.anisor = %f\n", materials[i].anisotropy_rotation);
		printf("  material.map_Ke = %s\n", materials[i].emissive_texname.c_str());
		printf("  material.map_Pr = %s\n", materials[i].roughness_texname.c_str());
		printf("  material.map_Pm = %s\n", materials[i].metallic_texname.c_str());
		printf("  material.map_Ps = %s\n", materials[i].sheen_texname.c_str());
		printf("  material.norm   = %s\n", materials[i].normal_texname.c_str());
		std::map<std::string, std::string>::const_iterator it(
			materials[i].unknown_parameter.begin());
		std::map<std::string, std::string>::const_iterator itEnd(
			materials[i].unknown_parameter.end());

		for (; it != itEnd; it++) {
			printf("  material.%s = %s\n", it->first.c_str(), it->second.c_str());
		}
		printf("\n");
	}
}



bool Demo::LoadObj(const char* filename, vector<Scalar> & vert, vector<int> & tri)
{
	std::cout << "Loading " << filename << std::endl;

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	//timerutil t;
	//t.start();
	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, filename, "", true);
	//t.end();
	//printf("Parsing time: %lu [msecs]\n", t.msec());

	if (!err.empty()) {
		std::cerr << err << std::endl;
	}

	if (!ret) {
		printf("Failed to load/parse .obj.\n");
		return false;
	}

	PrintInfoObj(attrib, shapes, materials);
	
	

	vert.clear();
	for (auto i : attrib.vertices)
		vert.push_back((Scalar)i);

	tri.clear();
	for (auto i = 0; i < shapes[0].mesh.indices.size() / 3; i++)
		for (auto j = 0; j < 3; j++)
		{
			tinyobj::index_t idx = shapes[0].mesh.indices[i * 3 + j];
			tri.push_back(idx.vertex_index);
		}

	return true;
}

bool Demo::LoadObj(const char* filename, vector<Scalar> & vert, vector<Scalar> & norm, vector<int> & tri)
{
	std::cout << "Loading " << filename << std::endl;

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	//timerutil t;
	//t.start();
	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, filename, "", true);
	//t.end();
	//printf("Parsing time: %lu [msecs]\n", t.msec());

	if (!err.empty()) {
		std::cerr << err << std::endl;
	}

	if (!ret) {
		printf("Failed to load/parse .obj.\n");
		return false;
	}

	PrintInfoObj(attrib, shapes, materials);



	vert.clear();
	for (auto i : attrib.vertices)
		vert.push_back((Scalar)i);

	norm.clear();
	for (auto i : attrib.normals)
		norm.push_back((Scalar)i);

	tri.clear();
	for (auto i = 0; i < shapes[0].mesh.indices.size() / 3; i++)
		for (auto j = 0; j < 3; j++)
		{
			tinyobj::index_t idx = shapes[0].mesh.indices[i * 3 + j];
			tri.push_back(idx.vertex_index);
		}


	return true;
}

/* Helper function to add serialized data to the scene */
void Demo::LoadSerializedHelper(const char* path)
{
	int num_objs = 0;
	int num_seams = 0;
	int num_colliders = 0;

	// Loads all the data from the file. Outputs the number of objects, seams and colliders
	dynamo::load_serialized_data(path, CoordinateSystem::CLIENT, num_objs, num_seams, num_colliders);

	// Add all the objects
	for (int i = 0; i < num_objs; i++) {
		int num_verts;
		int num_indices;
		int num_rv_distance;
		int num_rv_bending;

		// Retrieve information about the size of the arrays
		dynamo::retrieve_serialized_object_info(i, num_verts, num_indices, num_rv_distance, num_rv_bending);

		//Scalar *verts = NULL;
		//int *indices = NULL;
		//Scalar *rv_distance = NULL;
		//Scalar *rv_bending = NULL;
		//Scalar *kfriction = NULL;
		//Scalar *sfriction = NULL;
		//Scalar *invmass = NULL;
		float distance_stiffness;
		float bending_stiffness;

		vector<float> verts(num_verts);
		vector<int> indices(num_indices);
		vector<float> rv_distance(num_rv_distance);
		vector<float> rv_bending(num_rv_bending);
		vector<float> kfriction(num_verts / 3);
		vector<float> sfriction(num_verts / 3);
		vector<float> invmass(num_verts / 3);

		// Fill the arrays
		dynamo::retrieve_serialized_object_data(i, verts.data(), indices.data(),
												 rv_distance.data(), rv_bending.data(),
												 kfriction.data(), sfriction.data(), invmass.data(),
												 distance_stiffness, bending_stiffness);

		// Add the object
		int id;
		dynamo::create_deformable_object(verts.data(), num_verts / 3, indices.data(), num_indices, id);
		
		// Enable picking
		dynamo::set_picking_enabled(id, true);

		// Set the original rest values
		dynamo::set_object_rest_values(id, rv_distance.data(), num_rv_distance, rv_bending.data(), num_rv_bending);

		// Set the per vertex data
		for (int i = 0; i < num_verts / 3; i++) {
			dynamo::set_particle_friction_kinetic(id, i, kfriction[i]);
			dynamo::set_particle_friction_static(id, i, sfriction[i]);
			dynamo::set_particle_invmass(id, i, invmass[i]);
		}

		//delete[] verts;
		//delete[] indices;
		//delete[] rv_distance;
		//delete[] rv_bending;
		//delete[] kfriction;
		//delete[] sfriction;
		//delete[] invmass;
	}
	
	// Add all the seams
	for (int i = 0; i < num_seams; i++) 
	{
		int num_indices;

		// Retrieve information about the size of the array
		dynamo::retrieve_serialized_seam_info(i, num_indices);

		int object_id_a;
		int object_id_b;
		vector<int> indices(num_indices);
		float distance_stiffness;
		float bending_stiffness;

		int priority;

		// Fill the seam array and fetch additional data
		dynamo::retrieve_serialized_seam_data(i, object_id_a, object_id_b, indices.data(),
											   distance_stiffness, bending_stiffness, priority);

		// Add the seam
		int seam_id;
		dynamo::create_seam(object_id_a, object_id_b, indices.data(), num_indices, seam_id);
		dynamo::set_seam_damping(seam_id, false);

		dynamo::set_seam_distance_stiffness(seam_id, distance_stiffness);
		dynamo::set_seam_bending_stiffness(seam_id, bending_stiffness);

		//delete[] indices;
	}

	// Add all the colliders
	for (int i = 0; i < num_colliders; i++) 
	{
		// For each serialized collider, fetch the type
		char * type_c;
		type_c = new char[32];
		dynamo::retrieve_serialized_collider_info(i, type_c);
		string type(type_c);
		delete[] type_c;

		// Create a collider corresponding to the type
		if (type == "Box") {
			float mu_k;
			float mu_s;

			float min_x;
			float min_y;
			float min_z;

			float max_x;
			float max_y;
			float max_z;

			int id;
			dynamo::retrieve_serialized_aabb_collider_data(i, mu_k, mu_s, min_x, min_y, min_z, max_x, max_y, max_z);
			dynamo::create_axis_aligned_box_collider(max_x - min_x, max_y - min_y, max_z - min_z, mu_k, mu_s, id);
		}
		else if (type == "Capsule") {

			float mu_k;
			float mu_s;

			float p0_x;
			float p0_y;
			float p0_z;

			float p1_x;
			float p1_y;
			float p1_z;

			float radius0;
			float radius1;

			int id;
			dynamo::retrieve_serialized_capsule_collider_data(i, mu_k, mu_s, p0_x, p0_y, p0_z, p1_x, p1_y, p1_z, radius0, radius1);
			dynamo::create_capsule_collider(radius0, radius1, p0_x, p0_y, p0_z, p1_x, p1_y, p1_z, mu_k, mu_s, id);

		}
		else if (type == "Oriented Bounding Box") {

			float mu_k;
			float mu_s;

			float c_x;
			float c_y;
			float c_z;

			float x_x;
			float x_y;
			float x_z;

			float y_x;
			float y_y;
			float y_z;

			float z_x;
			float z_y;
			float z_z;

			float e_x;
			float e_y;
			float e_z;

			int id;

			dynamo::retrieve_serialized_obb_collider_data(i,
				mu_k, mu_s,
				c_x, c_y, c_z,
				x_x, x_y, x_z,
				y_x, y_y, y_z,
				z_x, z_y, z_z,
				e_x, e_y, e_z);

			// TODO: Pass center and normal, or translate + rotate?
			dynamo::create_object_oriented_box_collider(e_x *2, e_y * 2, e_z * 2, mu_k, mu_s, id);

		}
		else if (type == "Plane") {
			float mu_k;
			float mu_s;

			float pos_x;
			float pos_y;
			float pos_z;

			float nor_x;
			float nor_y;
			float nor_z;

			dynamo::retrieve_serialized_plane_collider_data(i, mu_k, mu_s, pos_x, pos_y, pos_z, nor_x, nor_y, nor_z);

			int id_plane;
			dynamo::create_plane_collider(nor_x, nor_y, nor_z, mu_k, mu_s, id_plane);
			dynamo::translate_collider(id_plane, pos_x, pos_y, pos_z);

		}
		else if (type == "Sphere") {
			float mu_k;
			float mu_s;

			float center_x;
			float center_y;
			float center_z;

			float radius;

			dynamo::retrieve_serialized_sphere_collider_data(i, mu_k, mu_s, radius, center_x, center_y, center_z);

			int id_sphere;
			dynamo::create_sphere_collider(radius, mu_k, mu_s, id_sphere);
			dynamo::translate_collider(id_sphere, center_x, center_y, center_z);
		}
	}
}