#ifndef __RENDERER__
#define __RENDERER__

#include <gl/glew.h>
#include <GLFW/glfw3.h>
#ifdef _WIN32
#undef APIENTRY
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL
#include <GLFW/glfw3native.h>
#endif

#include "cereal/cereal.hpp"
#include "types.h"

#include <string>

using namespace std;

#define MY_INFINITE 999999999

class Renderer
{
	int iters;
	int width, height;	// obsolete after porting to glfw, todo: remove

	float size_grid;

	bool show_axis;

	void DrawParticleSystem();
//	void DrawParticles(Vivace & vivace);
	void DrawParticles();
	//	void DrawColliders(Vivace & vivace);
//	void DrawColliders(Vivace & vivace);
	void DrawColliders();
	void DrawAxes(float length);
	void DrawGrid();
	void DrawBox(Vector3x min, Vector3x max);
	void DrawBox(Vector3x c, Vector3x u[3], Vector3x e);


	GLUquadricObj *quadric;	// used to draw spheres here and there (eg the light)

	GLhandleARB phongId;
	GLhandleARB phongBiasedId;
	GLhandleARB curvId;
	GLhandleARB curvBiasedId;
	GLhandleARB wireframeShaderId;
	GLhandleARB sphereShaderId;
	void loadShader(char* vshader, char* fshader, GLhandleARB & shaderId);
	GLhandleARB loadShaderCode(char* filename, unsigned int type);

	void setupPerspective(int width, int height);
	void setupMatrices(Vector3x position, Vector3x up, Vector3x lookAt);

	std::vector<Scalar> render_vertices;
	std::vector<Scalar> render_normals;
	std::vector<int>	render_indices;
	std::vector<int>	render_normal_indices;

	vector<Vector3x> b_end_points;
	vector<Vector3x> b_ctrl_points;
	vector<Vector3x> points;

	std::vector<std::vector<Vector3x>> bezier_points;
	std::vector<std::vector<Vector3x>> bezier_end_points;
	std::vector<std::vector<Vector3x>> bezier_control_points;


	int num_objects;
	vector<int> object_offsets;

	// video capture
	FILE* ffmpeg;	// the movie file
	int* pixels;	// buffer storing the current image. int because 4 bytes are need for rgba
	void InitVideoCapture();
	void UpdateVideoCapture();

	// [CUDA Interop]
	GLuint vbo;
	GLuint vbo_col;
	void createVBO(GLuint *vbo, GLuint *vbo_col);
	ComputationType ct;

	GLint cloth_bias_loc;
	GLint wireframe_bias_loc;
	GLint same_color_loc;

	int current_frame;

public:

	vector<float> bezier_points_curve_samples;
	vector<float> bezier_points_path_samples;
	int num_bezier_samples_per_curve;
	int num_bezier_samples_per_path;

	vector<float> bezier_points_paths;
	int num_bezier_curves_per_path;

	vector<float> bezier_points_specific;

	vector<int> specific_path_indices;

	static void glVertex3(const Vector3x & v);
	static void glTranslate(const Vector3x & v);
	static void glRotate(Scalar angle, const Vector3x & v);

	Vector3x GetRenderPos(int pi);
	Vector3x GetPhysicsPos(int pi);
	Vector3x GetNor(int pi);

	bool object_wireframe;
	bool wireframe_enabled;
	bool show_surface_particles = false;	// TODO: Remove assignment
	bool show_particles;

	bool show_normals;
	bool show_render_fps;
	bool show_grid;
	bool hide_mesh_collider = false;			// TODO: Remove assignment

	float zoom_factor;

	float camera_azimuth;
	float camera_fov;
	Vector3x p_camera;	//Camera position	
	Vector3x u_camera;	//Camera up	
	Vector3x r_camera;	//Camera right	
	Vector3x l_camera;	//Camera lookAt

	Vector3x p_light; // light position
	bool show_light;

	float cloth_bias;
	float wireframe_bias;
	bool same_color;

	GLdouble objectXform[4][4];	// used by the trackball

	void Init(int width, int height);

	void Draw(int width, int height);

	void DrawParticles(vector<unsigned int> & indices);

	void DrawMesh(Scalar * vert, unsigned int vert_num, int * tri, unsigned int idx_num);
	void DrawObjects();
	void Finalize();

	void Reshape(int width, int height);
	void Snapshot();
	void Snapshot(BYTE* array, unsigned int width, unsigned int height, const string & filename, bool topdown = false);
	void Shutdown();


	void Reset();
	size_t AddObject(vector<float> ver, vector<float> nor, vector<int> idx, bool wireframe);
	void UpdateObject(int id, vector<float> ver, vector<float> nor);

	void glPrint(float* c, float x, float y, float z, void * type, const char *fmt, ...);	// implementation of printf with GLUT
//	Particle * PickPart(ParticleSystem & ps, int x, int y,int w,int h, float & dist);

	void Get_Selection_Ray(int mouse_x, int mouse_y, double* p, double * q);

	void Select(double _p[], double _q[], int & selected_pi, double & dist);


	void UpdateCamera();
	void PrintCamera();

	bool export_to_snapshot;
	void ShutdownVideoCapture();

	template <class Archive>
	void serialize(Archive & archive)
	{
		archive(CEREAL_NVP(show_axis));
		archive(CEREAL_NVP(show_light));
		archive(CEREAL_NVP(show_grid));
		archive(CEREAL_NVP(size_grid));
		archive(CEREAL_NVP(show_particles));

		archive(CEREAL_NVP(show_normals));
		archive(CEREAL_NVP(show_render_fps));
		archive(CEREAL_NVP(wireframe_enabled));
		archive(CEREAL_NVP(export_to_snapshot));

		archive(
			cereal::make_nvp("light_pos", p_light)
			);
	}
};

#endif