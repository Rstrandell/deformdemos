#ifndef __DEMO_HANGING_CLOTH_
#define __DEMO_HANGING_CLOTH_

#include "../demo.h"
#include "dynamo_api.h"

class DemoHangingCloth : public Demo
{
public:

	const char * Name()
	{
		return "Hanging Cloth";
	}

	const char * Description()
	{
		return "This sample generates and animates a cloth hanged by the upper corners.";
	}

	void Init(Renderer * pRenderer)
	{
		int id_object, id_sphere;
		float size = 4.0f;
		int resolution = 81;

		int object_id;
		dynamo::create_deformable_patch(size, size, resolution, resolution, object_id);

		dynamo::translate_object(object_id, 0, -3, 0);

		dynamo::rotate_object(object_id, M_PI, 0, 1, 0);
		dynamo::rotate_object(object_id, M_PI, 0, 0, 1);


		dynamo::set_particle_invmass(object_id, 0, 0);
		dynamo::set_particle_invmass(object_id, resolution - 1, 0);
		dynamo::set_self_collision(true, false, false, 0.01f, 0.02f);

		/*int id_plane;
		dynamo::create_plane_collider(0, 1, 0, 0.5f, 1.25f, id_plane);*/
		int id_capsule;
		dynamo::create_capsule_collider(.25f, .5f, -4, 0, 0, 4, 0, 0, 0.01f, 0.02f, id_capsule);

		dynamo::init();
		dynamo::set_air_friction(0.0005f);
	}

	void Update(Renderer * pRenderer) {};
};

#endif