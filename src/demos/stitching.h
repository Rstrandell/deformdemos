#ifndef __DEMO_STITCHING__
#define __DEMO_STITCHING__

#include "../demo.h"

class DemoStitching : public Demo
{
private:
	int id_capsule;
	Vector3x caps_ep0;	// end point of the moving capsule
	Vector3x caps_ep1;	// end point of the moving capsule

public:

	const char * Name()
	{
		return "Stitching";
	}


	const char * Description()
	{
		return "Two patches are generated and sewed together\naround a capsule.";
	}


	void Init(Renderer* pRenderer)
	{
		caps_ep0 = Vector3x(-4, 0, 0);
		caps_ep1 = Vector3x(4, 0, 0);

		// Create the patches id0 and id1
		Scalar size_x = 7.f, size_y = 1.4f;
		unsigned int res_x = 140, res_y = 28;
		int id0, id1;

		{
			dynamo::create_deformable_patch(size_x, size_y, res_x, res_y, id0);
			dynamo::rotate_object(id0, 3.14f / 2.f, 1.f, 0.f, 0.f);
			dynamo::translate_object(id0, 0.f, 0.f, 0.6f);

			dynamo::set_distance_stiffness(id0, 0.8f);
			dynamo::set_bending_stiffness(id0, 0.0012f);
			dynamo::set_picking_enabled(id0, true);
		}
		{
			dynamo::create_deformable_patch(size_x, size_y, res_x, res_y, id1);
			dynamo::rotate_object(id1, 3.14f / 2.f, 1.f, 0.f, 0.f);
			dynamo::translate_object(id1, 0.f, 0.f, -0.6f);

			dynamo::set_distance_stiffness(id1, 0.8f);
			dynamo::set_bending_stiffness(id1, 0.0012f);
			dynamo::set_picking_enabled(id1, true);
		}

		// define the vertices ids to stitch together
		// in this case, they are defined by hand
		// in general, they have to be selected in some way from the UI

		// 3920 particles for each cloth (140 * 28) => sew together [0 - 139] with [3920 - 4059]
		vector<int> sew;
		for (unsigned int i = 0; i < res_x; i++)
		{
			// first line of id0 with first line of id1
			sew.push_back(i);
			sew.push_back(i);

			// last line of id0 with last line of id1
			sew.push_back(i + res_x * (res_y - 1));
			sew.push_back(i + res_x * (res_y - 1));
		}

		// Sew together along the contour
		int seam_id;
		dynamo::create_seam(id0, id1, &sew[0], (int)sew.size(), seam_id);
		dynamo::set_seam_distance_stiffness(seam_id, 1.0f);
		dynamo::set_seam_damping(seam_id, true);
		
		// Create a capsule collider
		dynamo::create_capsule_collider(.25f, .5f, caps_ep0[0], caps_ep0[1], caps_ep0[2], caps_ep1[0], caps_ep1[1], caps_ep1[2], 0.01f, 0.02f, id_capsule);

		// Add self-collision
		dynamo::set_self_collision(true, false, false, 0.01f, 0.02f);
		
		// Set air friction
		dynamo::set_air_friction(0.0005f);

		// Initialize the simulation
		dynamo::init();
	}

	void Update(Renderer* pRenderer)
	{
		// Start to rotate the capsule collider after 200 frames

		static int iter = 0;

		if (iter > 200)
			dynamo::rotate_collider(id_capsule, .001f, 0, 1, 0, 0, 0, 0);

		iter++;
	};


};

#endif