#ifndef __DEMO_COLLISION_OBBOX_
#define __DEMO_COLLISION_OBBOX_

#include "../demo.h"

class DemoCollisionOBBox : public Demo
{
	// The id of the box collider
	int id_box;
	int frame;

public:

	const char * Name()
	{
		return "Collision with an oriented bounding box";
	}


	const char * Description()
	{
		return "This sample shows how to set up and\nmove an oriented bounding box collider.";
	}


	void Init(Renderer* pRenderer)
	{
		pRenderer->p_camera = Vector3x(0.000f, 4.211f, -5.988f);
		pRenderer->u_camera = Vector3x(0.000f, 0.818f, 0.575f);
		pRenderer->r_camera = Vector3x(1.000f, 0.000f, 0.000f);
		pRenderer->l_camera = Vector3x(0.000f, 0.000f, 0.000f);
		pRenderer->camera_azimuth = -179.800f;

		unsigned int res = 101;
		frame = 0;
		
		// Create a patch and move it
		int id_object;
		dynamo::create_deformable_patch(3, 3, res, res, id_object);
		dynamo::translate_object(id_object, 0, 1.5f, 1.5f);

		// Enable picking
		dynamo::set_picking_enabled(id_object, true);

		// Set the bending stiffness of the patch
		dynamo::set_bending_stiffness(id_object, 0.0012f);

		// Fix two particles of the patch by setting their invmass to 0
		dynamo::set_particle_invmass(id_object, 0, 0);
		dynamo::set_particle_invmass(id_object, res - 1, 0);
		
		// Add self-collision
		dynamo::set_self_collision(true, false, false, 0.001f, 0.002f);

		dynamo::create_object_oriented_box_collider(6, 0.25f, 6, 0.5f, 1.25f, id_box);

		dynamo::set_air_friction(0.0005f);

		dynamo::init();		
	}

	void Update(Renderer* pRenderer) 	
	{	
		Scalar omega = Scalar(M_PI / 80);	// angular velocity
										    // rotation on plane xz
		if (frame < 400)
		{
			dynamo::rotate_collider(id_box, -omega, 0, 1, 0, 0, 0, 0);
			frame++;
		}
	}

};

#endif