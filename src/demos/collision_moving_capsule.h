#ifndef __DEMO_COLLISION_ROTATING_CAPSULE_
#define __DEMO_COLLISION_ROTATING_CAPSULE_

#include "../demo.h"

class DemoCollisionMovingCapsule : public Demo
{
private:
	int id_capsule;
	Vector3x caps_ep0;	// end point of the moving capsule
	Vector3x caps_ep1;	// end point of the moving capsule
	Scalar radius0;	// radii of the capsule's endpoints
	Scalar radius1;

	Scalar delta; // translation delta
	Scalar total_displacement;

public:

	const char * Name()
	{
		return "Collision with moving capsule";
	}


	const char * Description()
	{
		return "This sample shows how to instantiate a moving capsule collider.";
	}


	void Init(Renderer* pRenderer)
	{
		total_displacement = 0;
		delta = 0.04f;

		caps_ep0 = Vector3x(-4, -2, 0);
		caps_ep1 = Vector3x(4, -2, 0);
		radius0 = 0.25f;
		radius1 = 0.5f;
		unsigned int res = 71;

		// Create a patch, move it and set stiffnesses
		int id_object;
		dynamo::create_deformable_patch(4, 4, res, res, id_object);
		dynamo::translate_object(id_object, 0, 0, 2);

		// Enable picking
		dynamo::set_picking_enabled(id_object, true);

		dynamo::set_distance_stiffness(id_object, 1.f);
		dynamo::set_bending_stiffness(id_object, 0.012f);

		// Fix top row of particles in patch
		for (unsigned int pi = 0; pi < res; pi++)
		{
			dynamo::set_particle_invmass(id_object, pi, 0);
		}

		// Create a capsule collider and move it
		dynamo::create_capsule_collider(radius0, radius1, caps_ep0[0], caps_ep0[1], caps_ep0[2], caps_ep1[0], caps_ep1[1], caps_ep1[2], 0, 0, id_capsule);
		
		dynamo::set_self_collision(true, false, false, 0.01f, 0.02f);

		dynamo::set_air_friction(0.0005f);
		dynamo::init();
	}

	void Update(Renderer* pRenderer)
	{
		// Move capsule back and forth by updating z-values of the endpoints
		//Vector3x old_pos = caps_ep0 + (caps_ep1 - caps_ep0) * 0.5f;
		
		caps_ep0[2] += delta;
		caps_ep1[2] += delta;

		dynamo::update_capsule_collider(id_capsule, radius0, radius1, caps_ep0[0], caps_ep0[1], caps_ep0[2], caps_ep1[0], caps_ep1[1], caps_ep1[2]);
		//Vector3x new_pos = caps_ep0 + (caps_ep1 - caps_ep0) * 0.5f;
		//Vector3x d = new_pos - old_pos;
		
		total_displacement += delta;

		// Switch direction when total_displacement exceeds 4
		if ((abs(total_displacement) > 4)) 
			delta = -delta;
	};
};

#endif