#ifndef __DEMO_IMPLICIT_COLLIDERS_
#define __DEMO_IMPLICIT_COLLIDERS_

#include "../demo.h"
#include "dynamo_api.h"

class DemoImplicitColliders : public Demo
{
public:

	const char * Name()
	{
		return "Implicit Colliders";
	}

	const char * Description()
	{
		return "This sample shows the usage of all implicit colliders";
	}

	int box, capsule, sphere, plane;

	void Init(Renderer * pRenderer)
	{
		unsigned int res = 201;

		// Create a patch, then move and rotate it
		int id_object;
		dynamo::create_deformable_patch(11, 11, res, res, id_object);
		dynamo::translate_object(id_object, 0, 1, 0);
		dynamo::rotate_object(id_object, 3.141592f / 4.0f, 0, 1, 0);

		// Enable picking
		dynamo::set_picking_enabled(id_object, true);

		// Create and move implicit colliders
		dynamo::create_object_oriented_box_collider(1, 1, 1, 0.05f, 0.15f, box);
		dynamo::translate_collider(box, -3, 0, 0);

		dynamo::create_capsule_collider(0.5f, 0.5f, -1, 0, 0, 1, 0, 0, 0.3f, 0.9f, capsule);

		dynamo::create_sphere_collider(0.5f, 0.3f, 0.9f, sphere);
		dynamo::translate_collider(sphere, 3, 0, 0);

		dynamo::create_plane_collider(0, 1, 0, 0.3f, 0.9f, plane);
		dynamo::translate_collider(plane, 0, -2, 0);

		// Add self-collision
		dynamo::set_self_collision(true, false, false, 0.01f, 0.02f);

		dynamo::init();
		dynamo::set_air_friction(0.0005f);
	}

	void Update(Renderer * pRenderer) 
	{
		dynamo::rotate_collider(box,     0.02f, 1, 1, 1, 0, 0, 0);
		dynamo::rotate_collider(capsule, -0.02f, 1, 1, 1, 0, 0, 0);
		dynamo::rotate_collider(sphere,  0.02f, 1, 1, 1, 3, 0, 0);
	};
};

#endif