#ifndef __DEMO_FRICTION_SPHERE_
#define __DEMO_FRICTION_SPHERE_

#include "../demo.h"

class DemoFrictionSphere : public Demo
{
public:

	bool should_rotate;

	int id_sphere;

	const char * Name()
	{
		return "Friction with a sphere.";
	}

	const char * Description()
	{
		return "This sample shows the use of friction with a rotating sphere.";
	}

	void Init(Renderer* pRenderer)
	{
		should_rotate = true;

		unsigned int res = 101;
		
		// Create a patch, then move and rotate it
		int id_object;
		dynamo::create_deformable_patch(5.5, 5.5, res, res, id_object);
		dynamo::translate_object(id_object, 0.0f, 3.0f, 0);

		// Enable picking
		dynamo::set_picking_enabled(id_object, true);

		// Create sphere collider
		dynamo::create_sphere_collider(0.75f, 0.7f, 1.4f, id_sphere);
		dynamo::translate_collider(id_sphere, 0.0f, 1.15f, 0.0f);

		// Create plane collider
		int id_plane;
		dynamo::create_plane_collider(0, 1, 0, 0.5f, 1.25f, id_plane);

		// Add self-collision
		dynamo::set_self_collision(true, false, false, 0.0001f, 0.0002f);

		dynamo::set_air_friction(0.0005f);
		dynamo::set_gravity(0, -9.82f, 0);
		
		dynamo::init();
	}

	void Update(Renderer* pRenderer) {

		// Rotate the capsule collider around origin
		if (should_rotate) {
			dynamo::rotate_collider(id_sphere, ((M_PI * 2.f) / 200.f), 0.f, 1.f, 0.f, 0.f, 0.f, -0.0f);
		}
	};

	void KeyDown(int key)
	{
		switch (key)
		{
		case GLFW_KEY_E:
		{
			should_rotate = !should_rotate;
			break;
		}
		}
	}
};

#endif