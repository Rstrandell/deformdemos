#ifndef __DEMO_STACKING_
#define __DEMO_STACKING_

#include "../demo.h"

class DemoStacking : public Demo
{
public:

	int current_patch;
	int num_patches;
	unsigned int res;
	unsigned int frames_until_next_drop;

	const char * Name()
	{
		return "Stacking patches";
	}

	const char * Description()
	{
		return "This sample shows how to create a stack of patches";
	}

	void Init(Renderer* pRenderer)
	{
		current_patch = 0;
		num_patches = 28;
		res = 40;
		frames_until_next_drop = 0;

		for (auto i = 0; i < num_patches; i++)
		{
			int id_object;
			dynamo::create_deformable_patch(4, 4, res, res, id_object);
			dynamo::rotate_object(id_object, Scalar(M_PI * (0.3f + 0.15f * i)), 0, 1, 0);
			dynamo::translate_object(id_object, 0, Scalar(3 + ((Scalar(i) / 4.0f))), 0);

			dynamo::set_object_invmass(id_object, 0);

			// Enable picking
			dynamo::set_picking_enabled(id_object, true);
		}		

		// Add self collision
		dynamo::set_self_collision(true, false, false, 0.002f, 0.004f);

		// Create a plane collider
		int id_plane;
		dynamo::create_plane_collider(0, 1, 0, 0.6f, 0.8f, id_plane);

		// Initialize the simulation
		dynamo::init();

		// Set global parameters
		dynamo::set_gravity(0, -10, 0);
		dynamo::set_air_friction(0.0005f);
	}

	void Update(Renderer* pRenderer)
	{
		if (frames_until_next_drop <= 0 && current_patch < num_patches)
		{
			dynamo::set_object_invmass(current_patch, 1);
			current_patch++;

			frames_until_next_drop = (unsigned int) powf(2.0f, (float)(num_patches - current_patch) / 4.0f);
		}

		frames_until_next_drop--;
	}

	void KeyDown(int key)
	{
		/*switch (key)
		{
			case GLFW_KEY_N:
			{
				if (current_patch < num_patches)
				{
					deform::set_invmass_all_obj(current_patch, 1);
					current_patch++;
				}

				break;
			}
		}*/
	}
};

#endif