#ifndef __DEMO_COLLISION_BOX_
#define __DEMO_COLLISION_BOX_

#include "../demo.h"

class DemoCollisionBox : public Demo
{
	int id_box;

public:

	const char * Name()
	{
		return "Collision with a box";
	}


	const char * Description()
	{
		return "This sample shows how to set up a box collider.";
	}

	void Init(Renderer* pRenderer)
	{
		unsigned int res = 71;
		
		for (auto i = 0; i < 4; i++)
		{
			int id_object;
			dynamo::create_deformable_patch(4, 4, res, res, id_object);
			dynamo::rotate_object(id_object, Scalar(M_PI * (0.3f + 0.15f * i)), 0, 1, 0);
			dynamo::translate_object(id_object, 0, Scalar(3 + i), 0);

			// Enable picking
			dynamo::set_picking_enabled(id_object, true);
		}

		// Add self collision
		dynamo::set_self_collision(true, false, false, 0.002f, 0.004f);

		// Create a box collider
		dynamo::create_axis_aligned_box_collider(3, 0.5, 3, 0.01f, 0.02f, id_box);
		dynamo::translate_collider(id_box, 0, 0.25f, 0);
		
		// Create a plane collider
		int id_plane;
		dynamo::create_plane_collider(0, 1, 0, 0.6f, 0.8f, id_plane);

		// Initialize the simulation
		dynamo::init();

		// Set global parameters
		dynamo::set_gravity(0, -10, 0);
		dynamo::set_air_friction(0.0005f);
	}

	void Update(Renderer* pRenderer)
	{

	}
};

#endif