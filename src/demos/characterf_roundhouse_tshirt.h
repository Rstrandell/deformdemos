#ifndef __DEMO_CHARACTERF_ROUNDHOUSE_TSHIRT__
#define __DEMO_CHARACTERF_ROUNDHOUSE_TSHIRT__

#include "../demo.h"
#include <fstream>
#include "../animation_importer.h"

class DemoCharacterFRoundhouseTshirt : public Demo
{

public:

	vector<Scalar> c_vert;
	vector<Scalar> c_norm;
	vector<int> c_tri;
	AnimationImporter * anim_importer;

	int id_rendered_object;
	float t;

	bool animate;
	int id_coll;

	const char * Name()
	{
		return "Female Roundhouse pants and t-shirt";
	}

	const char * Description()
	{
		return "This sample shows how to load a serialized a deformable object.\nPress 'N' to start the character animation.";
	}

	void Init(Renderer* pRenderer)
	{
		animate = false;

		pRenderer->cloth_bias = 0.997f;
		pRenderer->wireframe_bias = 0.996f;
		pRenderer->same_color = true;

		LoadSerializedHelper(DATA_FOLDER "serialized/DemoCharacterFRoundhouseTshirt.def");

		// Add self-collision
		dynamo::set_self_collision(true, false, true, 0.1f, 0.15f);

		c_vert.clear();
		c_norm.clear();
		c_tri.clear();

		anim_importer = new AnimationImporter();

		anim_importer->LoadAnimation(DATA_FOLDER "female_roundhouse.fbx", c_vert, c_norm, c_tri, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.2f, 0.2f, 0.2f);

		dynamo::create_mesh_collider_remeshed(c_vert.data(), c_norm.data(), (int)c_vert.size() / 3, c_tri.data(), (int)c_tri.size(), 5000, 0.0045f, 0.25f, 1.0f, id_coll);

		t = 0;
		anim_importer->GetAnimationAtTime(t, c_vert, c_norm);
		dynamo::update_mesh_collider(id_coll, &(c_vert[0]), &(c_norm[0]));

		id_rendered_object = (int)pRenderer->AddObject(c_vert, c_norm, c_tri, false);
		pRenderer->hide_mesh_collider = true;

		dynamo::set_timesteps_per_frame(10);

		// Set air friction
		dynamo::set_air_friction(0.0005f);

		dynamo::set_solver_iterations(2);

		// Initialize the simulation
		dynamo::init();
	}

	void Update(Renderer* pRenderer)
	{
		pRenderer->UpdateObject(id_rendered_object, c_vert, c_norm);

		if (!animate) return;
		t += 0.016f;

		anim_importer->GetAnimationAtTime(t, c_vert, c_norm);

		dynamo::update_mesh_collider(id_coll, &(c_vert[0]), &(c_norm[0]));
	};

	void KeyDown(int key)
	{
		switch (key)
		{
		case GLFW_KEY_N:
		{
			animate = !animate;
			break;
		}
		case GLFW_KEY_1:
		{
			t += 0.012f;

			anim_importer->GetAnimationAtTime(t, c_vert, c_norm);
			dynamo::update_mesh_collider(id_coll, &(c_vert[0]), &(c_norm[0]));
			break;
		}
		}
	}

	void Destroy()
	{
		delete anim_importer;
	}
};

#endif