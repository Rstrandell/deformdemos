#ifndef __DEMO_LOAD_TETRA_MESH_
#define __DEMO_LOAD_TETRA_MESH_

#include "../demo.h"

class DemoLoadTetraMesh : public Demo
{
public:

	const char * Name()
	{
		return "Load Tetrahedral Mesh";
	}

	const char * Description()
	{
		return "This sample shows how to load an\nexternal tetrahedral, volumetric mesh from a MESH file.";
	}

	void Init(Renderer* pRenderer)
	{
		// Create deformable armadillo with tetrahedral constraints from .mesh file
		// This is currently the only way to add objects with tetrahedral constraints
		int id_object;
		dynamo::create_volumetric_deformable_object(DATA_FOLDER "armadillo_8k.mesh", id_object);

		// Scale, rotate and translate the armadillo
		dynamo::scale_object(id_object, .1f, .1f, .1f);
		dynamo::rotate_object(id_object, 3.14f, 0.f, 1.f, 0.f);
		dynamo::translate_object(id_object, 0, 6, 0);

		// Enable picking
		dynamo::set_picking_enabled(id_object, true);

		// Set stiffnesses. Tetra determines how well the object conserves its volume
		dynamo::set_distance_stiffness(id_object, 1.f);
		dynamo::set_bending_stiffness(id_object, 0.009f);
		dynamo::set_volume_stiffness(id_object, 1);

		// Add a plane collider
		int id_plane;
		dynamo::create_plane_collider(0, 1, 0, 0.01f, 0.02f, id_plane);

		// Add self collision
		dynamo::set_air_friction(0.002f);

		dynamo::init();
	}

	void Update(Renderer* pRenderer) {  };


};

#endif