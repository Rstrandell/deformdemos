#ifndef __DEMO_RIBBON_
#define __DEMO_RIBBON_

#include "../demo.h"
#include "dynamo_api.h"

class DemoRibbon : public Demo
{
public:

	const char * Name()
	{
		return "Ribbon";
	}


	const char * Description()
	{
		return "This sample generates and animates a ribbon falling on a plane.";
	}


	void Init(Renderer* pRenderer)
	{
		int id_object;
		dynamo::create_deformable_patch(1, 16, 25, 400, id_object);

		// Enable picking
		dynamo::set_picking_enabled(id_object, true);

		dynamo::set_bending_stiffness(id_object, 0.025f);

		dynamo::rotate_object(id_object, Scalar(-(M_PI * 0.9) / 2.0), Scalar(1), Scalar(0), Scalar(0));
		dynamo::translate_object(id_object, 0, 9, 0.f);

		int id_plane;
		dynamo::create_plane_collider(0, 1, 0, 0.1f, 1.55f, id_plane);

		dynamo::set_self_collision(true, false, false, 0.001f, 0.002f);

		dynamo::set_air_friction(0.003f);

		dynamo::init();
	}

	void Update(Renderer* pRenderer) {};


};

#endif