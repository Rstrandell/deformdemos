#ifndef __DEMO_COLLISION_MESH_SKINNED_REMESHED_
#define __DEMO_COLLISION_MESH_SKINNED_REMESHED_

#include "../demo.h"
#include "../animation_importer.h"

#include "dynamo_api.h"

class DemoCollisionMeshSkinnedRemeshed : public Demo
{
private:
	// Character mesh data
	vector<Scalar> c_vert;
	vector<Scalar> c_norm;
	vector<int> c_tri;

	AnimationImporter* anim_importer;

	bool animate;	// Set to false if you want the animation paused when starting the scene

	int id_coll;
	int id_rendered_object;

	float t;

	int frame;
	bool should_record = false;
	bool is_recording = false;

	bool global_skinning_enabled;

	int id_object;

public:

	const char * Name()
	{
		return "Mesh Collision Spinning";
	}

	const char * Description()
	{
		return "This sample collides a cloth with an animated mesh.\nThe mesh located in the imported FBX is remeshed to\nreduce the number of vertices from 51k to 2k.\nPress 'N' to toggle the animation.";
	}

	void Init(Renderer* pRenderer)
	{
		animate = false;

		pRenderer->cloth_bias = 0.995f;
		pRenderer->wireframe_bias = 0.994f;

		frame = 0;
		animate = false;

		vector<Scalar> d_vert;
		vector<int> d_tri;

		// Load t-shirt mesh
		bool result = LoadObj(DATA_FOLDER "t_shirt_fat_subdiv.obj", d_vert, d_tri);

		if (!result)
			return;

		// Create deformable object from t-shirt mesh
		dynamo::create_deformable_object(&(d_vert[0]), (int)d_vert.size() / 3, &(d_tri[0]), (int)d_tri.size(), id_object);

		// Enable picking
		dynamo::set_picking_enabled(id_object, true);

		// Add self collision
		dynamo::set_self_collision(true, true, false, 0.001f, 0.002f);
		dynamo::set_timesteps_per_frame(12);
		
		c_vert.clear();
		c_norm.clear();
		c_tri.clear();
		
		anim_importer = new AnimationImporter();
		anim_importer->LoadAnimation(DATA_FOLDER "spin.fbx", c_vert, c_norm, c_tri, 0.f, 0.f, 0.f, M_PI/4.f, 0.f, 1.f, 0.f, 0.3f, 0.3f, 0.3f);
		
		t = 0;
		
		anim_importer->GetAnimationAtTime(t, c_vert, c_norm);

		dynamo::create_mesh_collider(c_vert.data(), c_norm.data(), (int)c_vert.size() / 3, c_tri.data(), (int)c_tri.size(), 0.005f, 0.35f, 0.15f, id_coll);
		
		id_rendered_object = (int)pRenderer->AddObject(c_vert, c_norm, c_tri, false);
		pRenderer->hide_mesh_collider = true;

		vector<int> skinned = vector<int>(d_vert.size() / 3);
		
		dynamo::skin_to_mesh_collider(id_object, skinned.data(), (int)skinned.size(), id_coll);
		
		dynamo::init();
	}

	void Update(Renderer* pRenderer)
	{
		pRenderer->UpdateObject(id_rendered_object, c_vert, c_norm);

		if (!animate) return;

		t += 0.016f;

		anim_importer->GetAnimationAtTime(t, c_vert, c_norm);
		dynamo::update_mesh_collider(id_coll, &(c_vert[0]), &(c_norm[0]));
	};

	void KeyDown(int key)
	{
		switch (key)
		{
			case GLFW_KEY_N:
			{
				animate = !animate;
				break;
			}
			case GLFW_KEY_1:
			{
				t += 0.016f;

				anim_importer->GetAnimationAtTime(t, c_vert, c_norm);
				dynamo::update_mesh_collider(id_coll, &(c_vert[0]), &(c_norm[0]));
				break;
			}
			case GLFW_KEY_S:
			{
				global_skinning_enabled = !global_skinning_enabled;
				dynamo::set_global_skinning_enabled(id_object, global_skinning_enabled);
				break;
			}
		}
	}

	void Destroy()
	{
		delete anim_importer;
	}
};

#endif