#ifndef __DEMO_CHARACTERF_ROUNDHOUSE_TANKTOP__
#define __DEMO_CHARACTERF_ROUNDHOUSE_TANKTOP__

#include "../demo.h"
#include <fstream>
#include "../animation_importer.h"

class DemoCharacterFRoundhouseTanktop : public Demo
{

public:

	vector<Scalar> c_vert;
	vector<Scalar> c_norm;
	vector<int> c_tri;
	AnimationImporter * anim_importer;

	size_t id_rendered_object;
	float t;

	bool animate;
	size_t id_coll;

	const char * Name()
	{
		return "Female roundhouse pants and tanktop";
	}

	const char * Description()
	{
		return "This sample shows how to load a serialized a deformable object.\nPress 'N' to start the character animation.";
	}

	void Init(Renderer* pRenderer)
	{
		pRenderer->cloth_bias = 0.997f;
		pRenderer->wireframe_bias = 0.996f;
		pRenderer->same_color = true;

		LoadSerializedHelper(pVivace, DATA_FOLDER "serialized/DemoCharacterFRoundhouseTanktop.def");

		// Add self-collision
		size_t id_self = pVivace->add_coll_self(0.1f, 0.15f);
		pVivace->enable_surface_sampling(id_self);

		c_vert.clear();
		c_norm.clear();
		c_tri.clear();

		anim_importer = new AnimationImporter();

		anim_importer->LoadAnimation(DATA_FOLDER "female_roundhouse.fbx", c_vert, c_norm, c_tri, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.2f, 0.2f, 0.2f);

		id_coll = pVivace->add_coll_mesh_remeshed(c_vert.data(), c_norm.data(), c_vert.size(), c_tri.data(), c_tri.size(), 5000, 0.0045f, 0.25f, 1.0f);

		t = 0;
		anim_importer->GetAnimationAtTime(t, c_vert, c_norm);
		pVivace->update_coll_mesh(id_coll, &(c_vert[0]), &(c_norm[0]));

		id_rendered_object = pRenderer->AddObject(c_vert, c_norm, c_tri, false);
		pRenderer->hide_mesh_collider = true;

		pVivace->set_physics_iterations(10);

		// Set air friction
		pVivace->set_air_friction(0.0005f);

		// Initialize the simulation
		pVivace->Init();
	}

	void Update(Renderer* pRenderer)
	{
		pRenderer->UpdateObject(id_rendered_object, c_vert, c_norm);

		if (!animate) return;
		t += 0.016f;

		anim_importer->GetAnimationAtTime(t, c_vert, c_norm);

		pVivace->update_coll_mesh(id_coll, &(c_vert[0]), &(c_norm[0]));

	};

	void KeyDown(IVivace* pVivace, int key)
	{
		switch (key)
		{
		case GLFW_KEY_N:
		{
			animate = !animate;
			break;
		}
		case GLFW_KEY_1:
		{
			t += 0.012f;

			anim_importer->GetAnimationAtTime(t, c_vert, c_norm);
			pVivace->update_coll_mesh(id_coll, &(c_vert[0]), &(c_norm[0]));
			break;
		}
		}
	}

	void Destroy()
	{
		delete anim_importer;
	}
};

#endif