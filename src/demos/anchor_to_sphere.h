#ifndef __DEMO_ANCHOR_TO_SPHERE_
#define __DEMO_ANCHOR_TO_SPHERE_

#include "../demo.h"
#include "dynamo_api.h"

class DemoAnchorToSphere : public Demo
{
	// The id of the sphere collider
	int id_sphere;

	// The radius of the sphere collider
	Scalar radius;

	// Omega is used to rotate the sphere collider
	Scalar omega;

public:
	int id_object;

	const char * Name()
	{
		return "Anchor to sphere";
	}


	const char * Description()
	{
		return "This sample shows how anchor particles to a moving sphere collider.";
	}


	void Init(Renderer* pRenderer)
	{
		pRenderer->p_camera = Vector3x(0, 1, -15);
		pRenderer->l_camera = Vector3x(0, -1, 0);

		omega = 0;

		vector<Scalar> vert;
		vector<int> tri;

		bool res = LoadObj(DATA_FOLDER "t_shirt.obj", vert, tri);

		// Create a deformable object using the vertices and triangles of the skirt mesh
		dynamo::create_deformable_object(&(vert[0]), (int)vert.size() / 3, &(tri[0]), (int)tri.size(), id_object);



		// Add self collision
		dynamo::set_self_collision(true, false, true, 0.01f, 0.02f); //comment this out to test second problem


		// Initialize the simulation
		dynamo::init();

		// Set global parameters
		/*dynamo::set_air_friction(0.001f);*/
		dynamo::set_gravity(0, 0, 0);

		// Load a skirt mesh from an OBJ file
		//bool res = LoadObj(DATA_FOLDER "custom_skirt2.obj", vert, tri);

		//// Create a deformable object using the vertices and triangles of the skirt mesh
		//int id_object;
		//dynamo::create_deformable_object(&(vert[0]), (int)vert.size() / 3, &(tri[0]), (int)tri.size(), id_object);

		//// Scale up the deformable object 4 times
		//dynamo::scale_object(id_object, 4, 4, 4);

		//// Set the bending stiffness of the deformable object
		//dynamo::set_bending_stiffness(id_object, 0.00012f);

		//// Enable picking
		//dynamo::set_picking_enabled(id_object, true);

		//// Add self collision
		//dynamo::set_self_collision(true, false, true, 0.01f, 0.02f);

		//radius = 0.84f;

		//// Create a sphere collider
		//dynamo::create_sphere_collider(radius, 0.01f, 0.02f, id_sphere);

		//// Initialize the simulation
		//dynamo::init();

		//// Set global parameters
		//dynamo::set_air_friction(0.001f);
		////deform::set_gravity(0, 0, 0);

		//// Anchor the deformable object to the sphere collider
		//dynamo::anchor_object_to_collider(id_object, id_sphere);
	}

	void Update(Renderer* pRenderer)
	{
		// Move the sphere collider up/down and rotate
		/*if (omega > 0.064f * 100)
		{
			Scalar c_x = 0;
			Scalar c_y = sin(omega * 0.5f) * 0.5f;
			Scalar c_z = 0;
			dynamo::update_sphere_collider(id_sphere, radius, c_x, c_y, c_z);
			dynamo::rotate_collider(id_sphere, 0.03f, 0, 1, 0, c_x, c_y, c_z);
		}
		
		omega += 0.064f;*/
		dynamo::translate_object(id_object, -0.00001f, -0.009f, 0);
	}
};

#endif